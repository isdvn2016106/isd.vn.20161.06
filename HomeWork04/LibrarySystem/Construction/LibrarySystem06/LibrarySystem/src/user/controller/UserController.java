package user.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import borrowreturn.controller.BorrowReturnController;
import entity.BorrowRegistration;
import entity.User;

/**
 * Class này là lớp điều khiển người dùng
 * @author Sơn
 *
 */
public class UserController {
	/* Attributes */
	User user = new User();
	/* Constructors */
	private UserController() {
		
	}
	private static UserController inst;
	public static UserController getInst() {
		if (inst == null) {
			inst = new UserController();
		}
		return inst;
	}
	/* Methods */
	
	/**
	 * Hàm này trả về id theo tên tài khoản
	 * @param userName là tên tài khoản
	 * @return giá trị int là id
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int getIDByUsername(String userName) throws ClassNotFoundException, SQLException {
		return user.getIDByUsername(userName);
	}
	
	/**
	 * Hàm này trả về tên tài khoản theo id
	 * @param id là id tài khoản
	 * @return một String là tên tài khoản
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public String getUsernameByID(int id) throws ClassNotFoundException, SQLException {
		return user.getUsernameByID(id);
	}
	
	/**
	 * Hàm này trả về role của tài khoản
	 * @param userName là tên tài khoản
	 * @return một String là role của tài khoản
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public String getRoleByUserName(String userName) throws SQLException, ClassNotFoundException {
		return user.getRoleByUserame(userName);
	}
	
	/**
	 * Hàm này kiểm tra đăng nhập của người dùng
	 * @param userName là tên tài khoản
	 * @param password là mật khẩu
	 * @return kết quả đăng nhập
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public int checkLogin(String userName,String password) throws SQLException, ClassNotFoundException{
	       return user.checkLogin(userName, password);
	}
	
	/**
	 * Hàm này dùng để đăng ký tài khoản mới 
	 * @param user là tài khoản mới cần được đăng ký
	 * @return kết quả đăng ký
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean register(User user) throws ClassNotFoundException, SQLException{
        return user.addUser();
	}
	
	/**
	 * Hàm này tìm kiếm tài khoản theo tên tài khoản
	 * @param userName là tên tài khoản
	 * @return tài khoản tìm kiếm được
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public User findUserByUserName(String userName) throws SQLException, ClassNotFoundException{
        User user = new User();
        if(user.findByUserName(userName))
            return user;
        return null;
    }
	
	/**
	 * Hàm này tìm kiếm tài khoản người dùng theo các trường nhập vào
	 * @param userName là tên tài khoản
	 * @param fullName là tên đầy đủ của người dùng
	 * @param email là email người dùng
	 * @param role là role của người dùng
	 * @return một ArrayList các tài khoản
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public ArrayList<User> searchUser(String userName,String fullName, String email, String role) throws SQLException, ClassNotFoundException{
        User user = new User();
        return user.searchUser(userName, fullName, email, role);
    }
	
	/**
	 * Hàm này dùng để cập nhật tài khoản
	 * @param user
	 * @param oldEmail
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void updateProfile(User user,String oldEmail) throws SQLException, ClassNotFoundException{
        user.updateProfile(oldEmail);
    }

	/**
	 * Hàm này dùng để tìm kiếm tài khoản theo role
	 * @param role là role cần tìm kiếm
	 * @return một ArrayList các tài khoản 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<User> getUsersByRole(String role) throws ClassNotFoundException, SQLException {
		User user = new User();
		ArrayList<User> users = user.getUsersByRole(role);
		return users;
	}

	/**
	 * Hàm này dùng để cập nhật role của tài khoản
	 * @param username là tên tài khoản
	 * @param role là role cần cập nhật
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void updateRole(String username, String role) throws ClassNotFoundException, SQLException {
		User user = new User();
		user.findByUserName(username);
		user.setRole(role);
		user.updateProfile(username);
	}
	
	/**
	 * Hàm này tìm kiếm người dùng theo id
	 * @param id là id người dùng
	 * @return tài khoản người dùng cần tìm kiếm
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public User findUserById(int id) throws ClassNotFoundException, SQLException {
		User user = new User();
		if (user.findById(id)) {
			return user;
		}
		return null;
	}

}