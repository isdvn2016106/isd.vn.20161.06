package user.boundary;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.EtchedBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import book.controller.BookController;
import entity.Book;
import entity.SimpleQuery;

import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;

/**
 * Class này là lớp biên của chức năng kích hoạt tài khoản
 * @author Blej
 *
 */
public class ActivateAccountForm extends JPanel{
	private String userName;

	/**
	 * Create the panel.
	 */
	public ActivateAccountForm(String userName) {
		this.userName = userName;
		setLayout(null);
		initComponents();
	}

	private void initComponents() {
		this.setSize(800, 600);
		ActivateForm af = new ActivateForm(userName);
		af.setVisible(true);
		af.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
}
