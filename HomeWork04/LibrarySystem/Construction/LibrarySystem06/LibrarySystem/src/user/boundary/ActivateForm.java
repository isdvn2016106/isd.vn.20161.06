package user.boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import user.controller.CardController;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

/**
 * Class này là lớp biên của chức năng kích hoạt tài khoản
 * @author Sơn
 *
 */
public class ActivateForm extends JFrame {

	private JPanel contentPane;
	private JTextField txtActivationCode;
	private JButton btnActivate;
	private String userName;

	/**
	 * Create the frame.
	 */
	public ActivateForm(String userName) {
		this.userName = userName;
		setTitle("Activate Account");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 448, 127);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblActivationCode = new JLabel("Activation Code:");
		
		txtActivationCode = new JTextField();
		txtActivationCode.setColumns(10);
		
		btnActivate = new JButton("Activate");
		btnActivate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnActivateActionPerformed();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblActivationCode)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(txtActivationCode, GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(173)
							.addComponent(btnActivate)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblActivationCode)
						.addComponent(txtActivationCode, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(btnActivate)
					.addContainerGap(179, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}

	private void btnActivateActionPerformed() {
		try {
			String activationCode = txtActivationCode.getText();
			CardController.getInst().deactivateCard(userName);
			if (CardController.getInst().activateCard(userName, activationCode)) {
				JOptionPane.showMessageDialog(this, "Your account has been activated!", "Success!", JOptionPane.INFORMATION_MESSAGE);
				this.dispose();
			} else 
				JOptionPane.showMessageDialog(this, "Wrong Activation Code!", "Error!", JOptionPane.ERROR_MESSAGE);
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
