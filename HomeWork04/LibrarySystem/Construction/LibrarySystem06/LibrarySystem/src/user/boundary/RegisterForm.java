package user.boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.mysql.cj.fabric.jdbc.ErrorReportingExceptionInterceptor;

import entity.User;
import user.controller.UserController;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

/**
 * Class này là lớp biên của chức năng đăng ký
 * @author Sơn
 *
 */
public class RegisterForm extends JFrame {

	private JPanel contentPane;
	private JTextField txtUserName;
	private JPasswordField txtPassword;
	private JPasswordField txtConfirm;
	private JTextField txtContact;
	private JTextField txtStudentID;
	private JTextField txtStudyPeriod;
	private JTextField txtFullName;
	private ArrayList<String> errorMsgs;
	private JTextField txtEmail;
	private JComboBox cbbGender;
	private JComboBox cbbBorrowerType;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegisterForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterForm frame = new RegisterForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RegisterForm() {
		setTitle("Register");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 530);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblRegister = new JLabel("Register");
		lblRegister.setFont(new Font("Dialog", Font.BOLD, 14));
		
		JLabel lblUsername = new JLabel("Username: ");
		
		txtUserName = new JTextField();
		txtUserName.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password: ");
		
		txtPassword = new JPasswordField();
		
		JLabel lblConfirmPassword = new JLabel("Confirm Password: ");
		
		txtConfirm = new JPasswordField();
		
		JLabel lblGender = new JLabel("Gender: ");
		
		cbbGender = new JComboBox();
		cbbGender.setModel(new DefaultComboBoxModel(new String[] {"Male", "Female"}));
		
		JLabel lblContact = new JLabel("Contact: ");
		
		txtContact = new JTextField();
		txtContact.setColumns(10);
		
		JLabel lblBorrowerType = new JLabel("Borrower Type:");
		
		cbbBorrowerType = new JComboBox();
		cbbBorrowerType.setModel(new DefaultComboBoxModel(new String[] {"Student", "Non-Student"}));
		cbbBorrowerType.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				cbbBorrowerTypeItemStateChanged(e);
			}
		});
		
		JLabel lblStudentId = new JLabel("Student ID: ");
		
		txtStudentID = new JTextField();
		txtStudentID.setColumns(10);
		
		JLabel lblStudyPeriod = new JLabel("Study Period: ");
		
		txtStudyPeriod = new JTextField();
		txtStudyPeriod.setColumns(10);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSubmitActionPerformed(e);
			}
		});
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnCancelActionPerformed(e);
			}
		});
		
		JLabel lblFullName = new JLabel("Full Name:");
		
		txtFullName = new JTextField();
		txtFullName.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email: ");
		
		txtEmail = new JTextField();
		txtEmail.setColumns(10);
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(253)
							.addComponent(lblRegister))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(36)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblUsername)
								.addComponent(lblPassword)
								.addComponent(lblConfirmPassword)
								.addComponent(lblFullName)
								.addComponent(lblEmail)
								.addComponent(lblGender)
								.addComponent(lblContact)
								.addComponent(lblBorrowerType)
								.addComponent(lblStudentId)
								.addComponent(lblStudyPeriod))
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(cbbBorrowerType, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
									.addComponent(txtStudyPeriod, GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
										.addComponent(txtStudentID, Alignment.LEADING)
										.addComponent(txtFullName, Alignment.LEADING)
										.addComponent(txtConfirm, Alignment.LEADING)
										.addComponent(txtPassword, Alignment.LEADING)
										.addComponent(txtUserName, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 389, Short.MAX_VALUE)
										.addComponent(txtEmail, Alignment.LEADING)
										.addComponent(txtContact, Alignment.LEADING)))
								.addComponent(cbbGender, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(195)
							.addComponent(btnSubmit)
							.addGap(48)
							.addComponent(btnCancel)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(21)
					.addComponent(lblRegister)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblUsername)
						.addComponent(txtUserName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPassword)
						.addComponent(txtPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblConfirmPassword)
						.addComponent(txtConfirm, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblFullName)
						.addComponent(txtFullName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEmail)
						.addComponent(txtEmail, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(cbbGender, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblGender))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtContact, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblContact))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(cbbBorrowerType, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblBorrowerType))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtStudentID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblStudentId))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(txtStudyPeriod, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblStudyPeriod))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSubmit)
						.addComponent(btnCancel))
					.addContainerGap(90, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
		errorMsgs = new ArrayList<String>();
	}
	
	private void btnSubmitActionPerformed(ActionEvent e) {
		try {
            register();
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(this, "Occur error durring run: "+ex.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
        }
	}
	
	private void btnCancelActionPerformed(ActionEvent e) {
		RegisterLoginForm rlForm = new RegisterLoginForm();
        rlForm.setVisible(true);
        this.dispose();
	}
	


	private void cbbBorrowerTypeItemStateChanged(ItemEvent e) {
		String type = (String) e.getItem();
		if (type.equals("Student")) {
			txtStudentID.setEditable(true);
			txtStudyPeriod.setEditable(true);
		} else {
			txtStudentID.setEditable(false);
			txtStudyPeriod.setEditable(false);
		}
	}
	
	public boolean checkRegister() throws SQLException, ClassNotFoundException{
        String password = new String(txtPassword.getPassword());
        String confirmPass = new String(txtConfirm.getPassword());
        String contact = txtContact.getText();
        String borrowerType = cbbBorrowerType.getSelectedItem().toString();
        boolean hasError = !checkFullName();
        if(!AccountHelper.validateEmail(txtEmail.getText().toLowerCase())){
        	errorMsgs.add("Email invalid form!");
            hasError =true;
        }
        if(UserController.getInst().findUserByUserName(txtUserName.getText())!=null){
            hasError =true;
            errorMsgs.add("Email is already exist!");
        }
        if(password.length()<8){
            errorMsgs.add("Password must at leat 8 character!");
        }
        if(!AccountHelper.validatePassword(password)){
            errorMsgs.add("Password must have lower case, upper case, special character!");
            hasError= true;
        }
        if(!password.equals(confirmPass)){
            errorMsgs.add("Password did not match!");
            hasError= true;
        }
        if(!AccountHelper.validatePhoneNumber(contact)){
            errorMsgs.add("Phone number only have . - and space character!");
            hasError = true;
        }
        if(borrowerType.equals("Student")) {
        	if (txtStudentID.getText().equals("")) {
        		errorMsgs.add("You need to provide StudentID!");
        		hasError = true;
        	}
        	if (txtStudyPeriod.getText().equals("")) {
        		errorMsgs.add("You need to provide Study period!");
        		hasError = true;
        	}
        }
        return !hasError;
    }
	
	private boolean checkFullName(){
		switch (AccountHelper.validateName(txtFullName.getText())) {
		case 0:
			return true;
		case 1:
			errorMsgs.add("Full name can't be blank!");
			return false;
		default:
			errorMsgs.add("Full name is too long!");
			return false;
		}
    }
	
	private void register() throws SQLException, ClassNotFoundException{
		errorMsgs.clear();
        if(checkRegister()) addUser();
        else {
        	String error = "";
        	for (String msg : errorMsgs) {
        		error+=msg;
        		error+="\n";
        	}
        	JOptionPane.showMessageDialog(this, error, "Error!", JOptionPane.ERROR_MESSAGE);
        }
    }
	
	private void addUser(){
        try {
            User user = new User();
            String password = new String(txtPassword.getPassword());      
            String gender = (String) cbbGender.getItemAt(cbbGender.getSelectedIndex());
            String borrowerType = (String) cbbBorrowerType.getItemAt(cbbBorrowerType.getSelectedIndex());
            user.setEmail(txtEmail.getText().toLowerCase());
            user.setUserName(txtUserName.getText());
            user.setPassword(AccountHelper.MD5encrypt(password));
            user.setFullName(txtFullName.getText());
            user.setContact(txtContact.getText());
            user.setGender(gender);
            user.setBorrowerType(borrowerType);
            user.setStudentId(txtStudentID.getText());
            user.setStudyPeriod(txtStudyPeriod.getText());
            //user.setRole("Admin");
            //user.setRole("Librarian");
            user.setRole("Borrower");
            if(UserController.getInst().register(user)){
                int input = JOptionPane.showOptionDialog(null, "Success! Do you want to login in ?", "Register succes", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
                if(input == JOptionPane.OK_OPTION){
                    LoginForm loginForm = new LoginForm();
                    loginForm.setVisible(true);
                    this.dispose();
                } else {
                	RegisterLoginForm rlForm = new RegisterLoginForm();
                	rlForm.setVisible(true);
                	this.dispose();
                }           
            }else{
                JOptionPane.showMessageDialog(rootPane, "This email have existed");
            }
        } catch (SQLException ex) {
        	ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(this, "Occur error durring run: "+ex.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
        }
    }
}
