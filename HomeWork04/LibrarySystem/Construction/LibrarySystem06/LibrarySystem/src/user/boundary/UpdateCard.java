package user.boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DateFormatter;

import book.controller.BookController;
import entity.Book;
import entity.BorrowCard;
import entity.SimpleQuery;
import user.controller.CardController;
import user.controller.UserController;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JFormattedTextField;

/**
 * Class này là lớp biên của chức năng cập nhật thẻ
 * @author Sơn
 *
 */
public class UpdateCard extends JFrame {

	private static final String DATE_FORMAT = "dd-MM-yyyy";
	private ManageCardForm manageCardForm;
	private JPanel contentPane;
	private JTextField txtCardNum;
	private JTextField txtUsername;
	private ArrayList<String> errorMsgs;
	private JFormattedTextField txtExpiredDate;
	private BorrowCard card;

	/**
	 * Create the frame.
	 */
	public UpdateCard(BorrowCard card, ManageCardForm manageCardForm) {
		this.card = card;
		this.manageCardForm = manageCardForm;
		setTitle("Update Card");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 451, 180);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblCardNumber = new JLabel("Card Number:");
		
		txtCardNum = new JTextField();
		txtCardNum.setEditable(false);
		txtCardNum.setColumns(10);
		txtCardNum.setText(Integer.toString(card.getCardId()));
		
		JLabel lblUsername = new JLabel("Username:");
		
		txtUsername = new JTextField();
		txtUsername.setEditable(false);
		txtUsername.setColumns(10);
		try {
			txtUsername.setText(UserController.getInst().getUsernameByID(card.getUserId()));
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		JLabel lblExpiredDate = new JLabel("Expired Date:");
		DateFormat format = new SimpleDateFormat(DATE_FORMAT);
		txtExpiredDate = new JFormattedTextField(format);
		txtExpiredDate.setText(format.format(card.getExpiredDate()));
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSubmitActionPerformed();
			}
		});
		
		
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblCardNumber, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblExpiredDate, Alignment.TRAILING)
								.addComponent(lblUsername, Alignment.TRAILING))
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(4)
									.addComponent(txtCardNum, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
										.addComponent(txtExpiredDate)
										.addComponent(txtUsername, GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)))))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(171)
							.addComponent(btnSubmit, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(11, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(lblCardNumber))
						.addComponent(txtCardNum, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(txtUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(lblUsername)))
					.addGap(14)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblExpiredDate)
						.addComponent(txtExpiredDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(14)
					.addComponent(btnSubmit)
					.addGap(117))
		);
		contentPane.setLayout(gl_contentPane);
		errorMsgs = new ArrayList<String>();
	}
	
	protected void btnSubmitActionPerformed() {
		errorMsgs.clear();
        try {
			if(checkInput()) {
				updateCard();
				manageCardForm.genTableContent(manageCardForm.getTable(), CardController.getInst().getCards());
			}
			else {
				String error = "";
				for (String msg : errorMsgs) {
					error+=msg;
					error+="\n";
				}
				JOptionPane.showMessageDialog(null, error, "Error!", JOptionPane.ERROR_MESSAGE);
			}
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void updateCard() {
		try {
			BorrowCard card = this.card;
			DateFormat df = new SimpleDateFormat(DATE_FORMAT);
			Date inputDate = df.parse(txtExpiredDate.getText());
			card.setExpiredDate(new Timestamp(inputDate.getTime()));
			if (CardController.getInst().updateCard(card)) {
				JOptionPane.showMessageDialog(this, "Success!", "Update success!", JOptionPane.INFORMATION_MESSAGE);
			} else JOptionPane.showMessageDialog(this, "Error updating card!", "Error!", JOptionPane.ERROR_MESSAGE);
			this.dispose();
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Occur error durring run: "+e.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	private boolean checkInput() {
		boolean hasError = false;
		String inputDateString = txtExpiredDate.getText();
		if (inputDateString.equals("")) {
			errorMsgs.add("Expired date can't be blank!");
			hasError = true;
		} 
		try {
			DateFormat df = new SimpleDateFormat(DATE_FORMAT); 
			Date inputDate = df.parse(inputDateString);
			if (inputDate.compareTo(new Date()) < 0) {
				errorMsgs.add("Invalid input date!");
				hasError = true;
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return !hasError;
	}

}
