package borrowreturn.boundary;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SpringLayout;

/**
 * Hàm này là lớp biên của chức năng xem thông tin mượn 
 * @author Tùng
 *
 */
@SuppressWarnings("serial")
public class BorrowDetailForm extends JFrame {
	
	public BorrowDetailForm(String username, String contact, String bookTitle, String author, 
			String isbn, String bookId, String registeredDate) {
		SpringLayout springLayout = new SpringLayout();
		getContentPane().setLayout(springLayout);
		this.setSize(300, 300);
		
		JLabel Username = new JLabel("Username: " + username);
		springLayout.putConstraint(SpringLayout.WEST, Username, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, Username, -10, SpringLayout.EAST, getContentPane());
		getContentPane().add(Username);
		
		JLabel Contact = new JLabel("Contact: " + contact);
		springLayout.putConstraint(SpringLayout.SOUTH, Username, -6, SpringLayout.NORTH, Contact);
		springLayout.putConstraint(SpringLayout.EAST, Contact, 0, SpringLayout.EAST, Username);
		springLayout.putConstraint(SpringLayout.NORTH, Contact, 30, SpringLayout.NORTH, getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, Contact, 10, SpringLayout.WEST, getContentPane());
		getContentPane().add(Contact);
		
		JLabel BookTile = new JLabel("Book Title: " + bookTitle);
		springLayout.putConstraint(SpringLayout.NORTH, BookTile, 6, SpringLayout.SOUTH, Contact);
		springLayout.putConstraint(SpringLayout.WEST, BookTile, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, BookTile, 0, SpringLayout.EAST, Username);
		getContentPane().add(BookTile);
		
		JLabel Author = new JLabel("Author: " + author);
		springLayout.putConstraint(SpringLayout.WEST, Author, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, Author, 0, SpringLayout.EAST, Username);
		getContentPane().add(Author);
		
		JLabel ISBN = new JLabel("ISBN: " + isbn);
		springLayout.putConstraint(SpringLayout.SOUTH, Author, -6, SpringLayout.NORTH, ISBN);
		springLayout.putConstraint(SpringLayout.WEST, ISBN, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, ISBN, -157, SpringLayout.SOUTH, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, ISBN, 0, SpringLayout.EAST, Username);
		getContentPane().add(ISBN);
		
		JLabel Price = new JLabel("BookId: " + bookId);
		springLayout.putConstraint(SpringLayout.WEST, Price, 10, SpringLayout.WEST, getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, Price, 0, SpringLayout.EAST, Username);
		getContentPane().add(Price);
		
		
		JLabel RegisterDate = new JLabel("Registered Date: " + registeredDate);
		springLayout.putConstraint(SpringLayout.NORTH, RegisterDate, 6, SpringLayout.SOUTH, Price);
		springLayout.putConstraint(SpringLayout.WEST, RegisterDate, 0, SpringLayout.WEST, Username);
		springLayout.putConstraint(SpringLayout.EAST, RegisterDate, 0, SpringLayout.EAST, Username);
		getContentPane().add(RegisterDate);
	}
}
