package borrowreturn.boundary;

import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class AcceptReturnForm extends JPanel {
	private String username;

	public AcceptReturnForm(String username) {
		this.username = username;
		SearchLentForm slf = new SearchLentForm();
		slf.setVisible(true);
		slf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

}
