package borrowreturn.boundary;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.EtchedBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import book.controller.BookController;
import borrowreturn.controller.BorrowReturnController;
import entity.Book;
import entity.SimpleQuery;
import utilities.Constants;

import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;

/**
 * Hàm này là lớp biên của chức năng đăng ký mượn sách
 * @author Sơn
 *
 */
public class RegisterToBorrowForm extends JPanel{
	private String userName;
	private JTextField txtSearch;
	private JTable table;
	private JLabel lblAuthor;
	private JLabel lblTitle;
	private JLabel lblPublisher;
	private JLabel lblBookNumber;
	private JLabel lblISBN;
	private JLabel lblClassification;
	private JLabel lblStatus;
	private JLabel lblCopiesLeft;
	private JComboBox cbbFilter;
	private ArrayList<String> selectedBooks;
	private JButton btnSelect;
	private JLabel lblSelectedBook;
	private JLabel lblSelectedBooks;
	private JButton btnSubmit;

	/**
	 * Create the panel.
	 */
	public RegisterToBorrowForm(String userName) {
		this.userName = userName;
		initComponents();
	}

	private void initComponents() {
		this.setSize(800, 600);
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		
		JPanel panel_1 = new JPanel();
		
		lblSelectedBook = new JLabel("Selected books (0/5):");
		
		btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSubmitActionPerformed();
			}
		});
		
		lblSelectedBooks = new JLabel("");
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 782, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel, GroupLayout.DEFAULT_SIZE, 782, Short.MAX_VALUE)
							.addGap(10))))
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addGap(20)
					.addComponent(lblSelectedBook, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSelectedBooks, GroupLayout.DEFAULT_SIZE, 662, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(371, Short.MAX_VALUE)
					.addComponent(btnSubmit)
					.addGap(366))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 476, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSelectedBook)
						.addComponent(lblSelectedBooks))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnSubmit)
					.addGap(28))
		);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 0, 429, 477);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(447, 0, 325, 477);
		panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		JLabel label = new JLabel("Book Detail");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Dialog", Font.BOLD, 14));
		
		JLabel label_1 = new JLabel("Author:");
		
		lblAuthor = new JLabel("");
		
		JLabel label_3 = new JLabel("Publisher:");
		
		lblPublisher = new JLabel("");
		
		JLabel label_5 = new JLabel("Classification:");
		
		lblClassification = new JLabel("");
		
		JLabel label_7 = new JLabel("ISBN:");
		
		lblISBN = new JLabel("");
		
		lblStatus = new JLabel("");
		
		lblCopiesLeft = new JLabel("");
		
		JLabel label_15 = new JLabel("Book Number:");
		
		JLabel label_16 = new JLabel("Title:");
		
		lblTitle = new JLabel("");
		
		lblBookNumber = new JLabel("");
		
		btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSelectActionPerformed();
			}
		});
		
		JLabel lblStatus_1 = new JLabel("Status:");
		
		JLabel lblNewLabel_1 = new JLabel("Copies Left:");
		
		JButton btnDeselect = new JButton("Deselect ");
		btnDeselect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnDeselectActionPerformed();
			}
		});
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING)
								.addComponent(label_15)
								.addComponent(label_16)
								.addComponent(label_1)
								.addComponent(label_3)
								.addComponent(label_5)
								.addComponent(label_7)
								.addComponent(lblStatus_1)
								.addComponent(lblNewLabel_1))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAuthor, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblPublisher, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblClassification, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblISBN, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblTitle, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
								.addComponent(lblBookNumber, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
								.addComponent(lblStatus, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblCopiesLeft, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_panel_2.createSequentialGroup()
									.addComponent(btnSelect, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(btnDeselect))))
						.addComponent(label, GroupLayout.PREFERRED_SIZE, 321, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addComponent(label)
					.addGap(18)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_15)
						.addComponent(lblBookNumber))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_16)
						.addComponent(lblTitle))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(lblAuthor))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_3)
						.addComponent(lblPublisher))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_5)
						.addComponent(lblClassification))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_7)
						.addComponent(lblISBN))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(lblStatus)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblStatus_1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_1)
								.addComponent(lblCopiesLeft))))
					.addPreferredGap(ComponentPlacement.RELATED, 237, Short.MAX_VALUE)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSelect)
						.addComponent(btnDeselect))
					.addContainerGap())
		);
		panel_2.setLayout(gl_panel_2);
		panel_1.setLayout(null);
		
		table = new JTable();
		tableInit();
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		panel_1.add(scrollPane);
		panel_1.add(panel_2);
		ListSelectionModel rowSelectionModel = table.getSelectionModel();
		rowSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		rowSelectionModel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				rowValueChanged(e);
			}
		});
		
		JLabel lblSearchBooks = new JLabel("Search Books:");
		
		txtSearch = new JTextField();
		txtSearch.setColumns(10);
		
		JButton btnSe = new JButton("Search");
		btnSe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSearchActionPerformed();
			}
		});
		
		cbbFilter = new JComboBox();
		cbbFilter.setModel(new DefaultComboBoxModel(new String[] {"Title", "Author", "Publisher", "Classification"}));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSearchBooks)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtSearch, GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(cbbFilter, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnSe)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSearchBooks)
						.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSe)
						.addComponent(cbbFilter, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(15, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		setLayout(groupLayout);
		selectedBooks = new ArrayList<String>();
	}

	private void btnDeselectActionPerformed() {
		deselectBooks(lblBookNumber.getText());		
	}

	private void deselectBooks(String bookNumber) {
		if (!selectedBooks.contains(bookNumber)) {
			JOptionPane.showMessageDialog(this, "You have to select it first!", "Error!", JOptionPane.ERROR_MESSAGE);
		} else {
			selectedBooks.remove(bookNumber);
			lblSelectedBook.setText("Selected books ("+selectedBooks.size()+"/5):");
			lblSelectedBook.repaint();
			String books = "";
			for (int index = 0; index < selectedBooks.size(); index++) {
				books += selectedBooks.get(index)+",";
			}
			lblSelectedBooks.setText(books);
			lblSelectedBooks.repaint();
		}
	}

	private void btnSubmitActionPerformed() {
		if (selectedBooks.size() == 0) {
			JOptionPane.showMessageDialog(this, "You have to select atleast one book!", "Error!", JOptionPane.ERROR_MESSAGE);
		} else {
			try {
				int result = BorrowReturnController.getInst().registerToBorrow(selectedBooks, userName);
				switch (result) {
				case Constants.CARD_EXPIRED:
					JOptionPane.showMessageDialog(this, "Your card has expired!", "Error!", JOptionPane.ERROR_MESSAGE);
					break;
				case Constants.HAS_UNRETURNED_BOOK:
					JOptionPane.showMessageDialog(this, "You have unreturned book(s)!", "Error!", JOptionPane.ERROR_MESSAGE);
					break;
				case Constants.REGISTER_TO_BORROW_FAILED:
					JOptionPane.showMessageDialog(this, "Oops! Someone has faster hands than you! No more copy left!", "Error!", JOptionPane.ERROR_MESSAGE);
					break;
				default:
					JOptionPane.showMessageDialog(this, "Register success! Please come to library within 2 days to retrieve the books!", "Error!", JOptionPane.INFORMATION_MESSAGE);
					break;
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	private void btnSelectActionPerformed() {
		selectBook(lblBookNumber.getText());
	}

	private void selectBook(String bookNumber) {
		if (selectedBooks.size() == 5) {
			JOptionPane.showMessageDialog(this, "You have selected 5 books!", "Error!", JOptionPane.ERROR_MESSAGE);
		} else if (selectedBooks.contains(bookNumber)) {
			JOptionPane.showMessageDialog(this, "You have selected this book already!", "Error!", JOptionPane.ERROR_MESSAGE);
		} else {
			selectedBooks.add(bookNumber);
			lblSelectedBook.setText("Selected books ("+selectedBooks.size()+"/5):");
			lblSelectedBook.repaint();
			String books = "";
			for (int index = 0; index < selectedBooks.size(); index++) {
				books += selectedBooks.get(index)+",";
			}
			lblSelectedBooks.setText(books);
			lblSelectedBooks.repaint();
		}
	}

	private void tableInit() {
		try {
			genTableContent(table,BookController.getInst().getBooks());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void btnSearchActionPerformed() {
		try {
			String filter = cbbFilter.getSelectedItem().toString();
			String input = txtSearch.getText();
			ArrayList<Book> books = BookController.getInst().getBooksByFilter(input, filter);
			genTableContent(table, books);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void genTableContent(JTable table, ArrayList<Book> books){
        try {
            Vector<String> row,colunm;
            int count =1;
            DefaultTableModel model = new DefaultTableModel();
            String[] colunmNames = {"Book Number", "Title", "Author", "Publisher", "Classification"};
            colunm = new Vector<String>();
            int numberColumn;
            numberColumn = colunmNames.length;
            for(int i =0; i<numberColumn;i++){
                colunm.add(colunmNames[i]);
            }   
            model.setColumnIdentifiers(colunm);
            for(Book book : books){
                row = new Vector<String>();
                row.add(book.getBookNumber());
                row.add(book.getTitle());
                row.add(book.getAuthor());
                String publisher = BookController.getInst().findNameById(SimpleQuery.PUBLISHER, book.getPublisherId());
                String classification = BookController.getInst().findNameById(SimpleQuery.CLASSIFICATION, book.getClassificationId());
                row.add(publisher);
                row.add(classification);
                count++;
                model.addRow(row);
            }   table.setModel(model);
            table.setVisible(true);
        } catch (SQLException ex) {
        	ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(this, "Occur error durring run: "+ex.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
        }
    }   

	private void rowValueChanged(ListSelectionEvent e) {
		try {
			int rowNumber = table.getSelectedRow();
			if (rowNumber >= 0) {
				String bookNumber = (String) table.getValueAt(table.getSelectedRow(), 0);
				Book book = BookController.getInst().findBookByBookNumber(bookNumber);
				lblBookNumber.setText(bookNumber);
				lblTitle.setText(book.getTitle());
				lblAuthor.setText(book.getAuthor());
				lblISBN.setText(book.getISBN());
				lblPublisher.setText(BookController.getInst().findNameById(SimpleQuery.PUBLISHER, book.getPublisherId()));
				lblClassification.setText(BookController.getInst().findNameById(SimpleQuery.CLASSIFICATION, book.getClassificationId()));
				int count = BookController.getInst().getBorrowableCopyCount(bookNumber);
				if (count > 0) {
					lblStatus.setText("Borrowable");
					lblCopiesLeft.setText(Integer.toString(count));
					btnSelect.setEnabled(true);
					
				} else {
					lblStatus.setText("Not borrowable");
					lblCopiesLeft.setText("0");
					btnSelect.setEnabled(false);
				}
			} else return;
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	public JTable getTable() {
		return table;
	}
}
