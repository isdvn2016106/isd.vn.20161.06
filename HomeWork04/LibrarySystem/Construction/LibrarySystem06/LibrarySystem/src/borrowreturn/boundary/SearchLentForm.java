package borrowreturn.boundary;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import book.controller.BookController;
import borrowreturn.controller.BorrowReturnController;
import entity.Book;
import entity.BookCopy;
import entity.BorrowCard;
import entity.BorrowDetail;
import entity.BorrowRegistration;
import entity.User;
import user.controller.UserController;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import java.awt.FlowLayout;
import java.awt.HeadlessException;

import javax.swing.JTable;

/**
 * Hàm này là lớp biên của chức năng tìm kiếm sách mượn
 * @author Huy
 *
 */
@SuppressWarnings("serial")
public class SearchLentForm extends JFrame implements ActionListener, 
	ListSelectionListener {
	private static JFrame sFrame;
	private JTextField mTextField;
	private JButton mSearchButton;
	JScrollPane mResultPanel;
	private JTable mTableView;
	private JPanel mFooter;
	private JButton mAcceptButton;
	private JButton mDeclineButton;
	private BorrowDetailForm mDetailForm;
	private int mCardId;
	private BorrowRegistration registration;
	
	public SearchLentForm() {
		getContentPane().setLayout(new BorderLayout(0, 0));
		this.setSize(600, 600);
		this.setVisible(true);
		
		JPanel searchGroup = new JPanel();
		getContentPane().add(searchGroup, BorderLayout.NORTH);
		searchGroup.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lblNewLabel = new JLabel("Enter Card Id");
		searchGroup.add(lblNewLabel);
		
		mTextField = new JTextField();
		searchGroup.add(mTextField);
		mTextField.setColumns(10);
		
		mSearchButton = new JButton("Search");
		mSearchButton.addActionListener(this);
		searchGroup.add(mSearchButton);
		
		mResultPanel = new JScrollPane();
		getContentPane().add(mResultPanel, BorderLayout.CENTER);
		
		mFooter = new JPanel();
		getContentPane().add(mFooter, BorderLayout.SOUTH);
		mFooter.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		mAcceptButton = new JButton("Accept");
		mAcceptButton.addActionListener(this);
		mFooter.add(mAcceptButton);
/*		
		mDeclineButton = new JButton("Decline All");
		mDeclineButton.addActionListener(this);
		mFooter.add(mDeclineButton);*/
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                sFrame = new SearchLentForm();
            }
        });
	}
	
	@Override
	public void valueChanged(ListSelectionEvent e) {		
		try {
			if (!e.getValueIsAdjusting()) {
				String username = mTableView.getValueAt(mTableView.getSelectedRow(), 0).toString();
				String bookTitle = mTableView.getValueAt(mTableView.getSelectedRow(), 1).toString();
				String registeredDate = mTableView.getValueAt(mTableView.getSelectedRow(), 2).toString();
				
				User user = UserController.getInst().findUserByUserName(username);
				Book book = BookController.getInst().findBookByTitle(bookTitle);
				
				SwingUtilities.invokeLater(new Runnable() {
		            @Override
		            public void run() {
		            	if (mDetailForm != null) mDetailForm.dispose();
		            	mDetailForm = new BorrowDetailForm(username, user.getContact(), bookTitle, 
		            			book.getAuthor(), book.getISBN(), book.getBookNumber(), registeredDate);
		            	mDetailForm.setVisible(true);
		            }
		        });
			}
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Search")) {
			try {
				mCardId = Integer.parseInt(mTextField.getText());
				ArrayList<BorrowRegistration> list = BorrowReturnController.getInst()
						.searchBorrowRegistrationByCardId(1, mCardId);
				String[] columns = new String[] {"Username", "Book Title", 
						"Register Date"};
				int count = 0;
				for (BorrowRegistration borrowRegistration : list) {
					int borrowId = borrowRegistration.getBorrowId();
					count += this.getBookTitleFromBorrowId(borrowId).size();
					this.registration = borrowRegistration;
				}
				Object[][] data = new Object[count][5];
				count = 0;
				ArrayList<String> bookTitles;
				for (BorrowRegistration borrowRegistration : list) {
					int borrowId = borrowRegistration.getBorrowId();
					String username = this.getUsernameFromCardId(mCardId);
					bookTitles = this.getBookTitleFromBorrowId(borrowId);
					String registeredDate = borrowRegistration.getRegisteredDate() + "";
					for (String title : bookTitles) {
						data[count][0] = username;
						data[count][1] = title;
						data[count][2] = registeredDate;
						count ++;
					}
				}
		        mTableView = new JTable(data, columns);
		        mTableView.getSelectionModel().addListSelectionListener(this);
		        
				mResultPanel.setViewportView(mTableView);
			} catch (NumberFormatException | ClassNotFoundException | SQLException exception) {
				exception.printStackTrace();
				mCardId = 0;
			}
		} else if (e.getActionCommand().equals("Accept")) {			
			Calendar c = Calendar.getInstance();
			Timestamp returnDate = new Timestamp(c.getTime().getTime());
			try {
				if (BorrowReturnController.getInst().updateBorrowRegistrationStatus(2, registration.getBorrowId()) > 0 
						&& BorrowReturnController.getInst().updateBorrowRegistrationReturnDate(returnDate, registration.getBorrowId()) > 0) {
					BorrowReturnController.getInst().makeCopyAvailable(registration.getBorrowId());
					mCardId = 0;
					JOptionPane.showMessageDialog(sFrame,
						    "Success!");
					this.dispose();
				} else {
					JOptionPane.showMessageDialog(sFrame,
						    "Nothing to update!");
				}
			} catch (HeadlessException | ClassNotFoundException | SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	private String getUsernameFromCardId(int cardId) throws ClassNotFoundException, SQLException {
		String name = "";
		BorrowCard borrowCard = new BorrowCard();
		if (borrowCard.findByCardId(cardId)) {
			name = UserController.getInst().findUserById(borrowCard.getUserId()).getUserName();
			if (name.isEmpty()) {
				name = UserController.getInst().findUserById(borrowCard.getUserId()).getUserName();
			}
		}
		return name;
	}
	
	private ArrayList<String> getBookTitleFromBorrowId(int borrowId) throws ClassNotFoundException, 
		SQLException {
		ArrayList<String> listTitle = new ArrayList<String>();
		BorrowDetail borrowDetail = new BorrowDetail();
		ArrayList<BorrowDetail> list = borrowDetail.findByBorrowId(borrowId);
		BookCopy bookCopy = new BookCopy();
		for (BorrowDetail bd : list) {
			if (bookCopy.findByCopyId(bd.getCopyId())) {
				listTitle.add(BookController.getInst().findBookByBookNumber(bookCopy.getBookId())
						.getTitle());
			}
		}
		return listTitle;
	}
}
