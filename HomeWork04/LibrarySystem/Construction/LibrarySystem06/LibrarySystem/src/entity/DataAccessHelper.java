package entity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class này giúp tạo hoặc đóng kết nối với cơ sở dữ liệu
 * @author Sơn
 *
 */
public class DataAccessHelper {
	private final static String GET_COUNT = "select count(*) from ";
	private final static String hostName = "localhost";
	private final static String dbName = "mydb";
	private final static String userName = "root";
	private final static String password = "root";
    private static String dbPath = "jdbc:mysql://"+hostName+":3306/"+dbName+"?characterEncoding=utf8";
    public static Connection conn = null;
    
    /**
     * Hàm này để tạo kết nối với cơ sở dữ liệu
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void connectDB() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        conn = DriverManager.getConnection(dbPath, userName, password);
    }
    
    /**
     * Hàm này để đóng kết nối cơ sở dữ liệu
     * @throws SQLException
     */
    public void closeDB() throws SQLException {
        if (conn != null)
        	conn.close();
    }
   
    public void setDbPath(String dbPath){
        DataAccessHelper.dbPath = dbPath;
    }
}
