package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Class này quản lý 2 bảng Publihser và Classification trong cớ sở dữ liệu
 * @author Sơn
 *
 */
public class SimpleQuery extends DataAccessHelper{
	public static final int PUBLISHER = 0;
	public static final int CLASSIFICATION = 1;
	private static final String GET_ALL_PUBLISHER = "select * from `publisher`";
	private static final String GET_ALL_CLASSIFICATION = "select * from `classification`";
	private static final String SEARCH_PUBLISHERID_BY_NAME = "select publisherId from `publisher` where publisherName like ?";
	private static final String SEARCH_PUBLISHERNAME_BY_ID = "select publisherName from `publisher` where publisherId = ?";
	private static final String SEARCH_CLASSIFICATIONID_BY_NAME = "select classificationId from `classification` where classificationName like ?";
	private static final String SEARCH_CLASSIFICATIONNAME_BY_ID = "select classificationName from `classification` where classificationId = ?";
	private static final String SEARCH_CLASSIFICATIONCODE_BY_NAME = "select classificationCode from `classification` where classificationName like ?";
	
	/**
	 * Hàm này trả về toàn bộ một cột trong cơ sở dữ liệu của bảng Publihser hoặc Classification 
	 * @param tableName là tên bảng Publisher hoặc Classification 
	 * @param column là tên cột
	 * @return một ArrayList kết quả
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<String> getAll(int tableName, String column) throws ClassNotFoundException, SQLException {
		ArrayList<String> items = new ArrayList<String>();
		connectDB();
		String query;
		if (tableName == SimpleQuery.PUBLISHER) query = GET_ALL_PUBLISHER;
		else query = GET_ALL_CLASSIFICATION;
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		if (rs != null) {
			while (rs.next()) {
				items.add(rs.getString(column));
			}
		}
		closeDB();
		return items;
	}

	/**
	 * Hàm này trả về id theo tên
	 * @param tableName là tên bảng Publisher hoặc Classification
	 * @param name là tên
	 * @return giá trị int là id
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int findIdByName(int tableName, String name) throws ClassNotFoundException, SQLException {
		int id = 0;
		connectDB();
		String query;
		if (tableName == SimpleQuery.PUBLISHER) query = SEARCH_PUBLISHERID_BY_NAME;
		else query = SEARCH_CLASSIFICATIONID_BY_NAME;
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, name);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			id = rs.getInt(1);
		}
		closeDB();
		return id;
	}
	
	/**
	 * Hàm này trả về tên theo id
	 * @param tableName là tên bảng Publisher hoặc Classification
	 * @param id là id
	 * @return một String là tên
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public String findNamebyId(int tableName, int id) throws ClassNotFoundException, SQLException {
		String name = "";
		connectDB();
		String query;
		if (tableName == SimpleQuery.PUBLISHER) query = SEARCH_PUBLISHERNAME_BY_ID;
		else query = SEARCH_CLASSIFICATIONNAME_BY_ID;
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			name = rs.getString(1);
		}
		closeDB();
		return name;
	}
	
	/**
	 * Hàm này trả về ClassificationCode theo tên
	 * @param name là tên classification
	 * @return một String là code
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public String findClassificationCodebyName(String name) throws ClassNotFoundException, SQLException {
		String code = "";
		connectDB();
		String query = SEARCH_CLASSIFICATIONCODE_BY_NAME;
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setString(1, name);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			code = rs.getString(1);
		}
		closeDB();
		return code;
	}
}
