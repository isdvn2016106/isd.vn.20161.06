package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Class này quản lý BorrowDetail trong cơ sở dữ liệu
 * @author Sơn
 *
 */
public class BorrowDetail extends DataAccessHelper{
	private final String SEARCH_BORROW_DETAIL_BY_BORROW_ID = "select * from `borrowdetail` where borrowId = ?";
	private static final String ADD_DETAIL = "INSERT INTO `borrowDetail` (borrowId, copyId) VALUES (?,?)";
	private static final String SEARCH_DETAILS = "SELECT * FROM `borrowDetail` WHERE borrowId = ?";
	
	private int detailId;
	private int borrowId;
	private String copyId;
	
	/**
	 * Hàm này tìm kiếm chi tiết đăng ký mượn sách
	 * @param borrowId là id của đăng ký mượn sách
	 * @return một ArrayList các chi tiết
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<BorrowDetail> findByBorrowId(int borrowId) throws ClassNotFoundException, SQLException {
		connectDB();
    	PreparedStatement ps = conn.prepareStatement(SEARCH_BORROW_DETAIL_BY_BORROW_ID);
    	ps.setString(1, borrowId + "");
    	ResultSet rs = ps.executeQuery();
    	ArrayList<BorrowDetail> list = new ArrayList<BorrowDetail>();
    	if (rs != null) {
    		while (rs.next()) {
    			BorrowDetail borrowDetail = new BorrowDetail();
    			borrowDetail.setBorrowId(rs.getInt("borrowId"));
    			borrowDetail.setCopyId(rs.getString("copyId"));
    			borrowDetail.setDetailId(rs.getInt("detailId"));
    			list.add(borrowDetail);
    		}
    	}
    	closeDB();
    	return list;
	}
	
	public int getDetailId() {
		return detailId;
	}
	public void setDetailId(int detailId) {
		this.detailId = detailId;
	}
	public int getBorrowId() {
		return borrowId;
	}
	public void setBorrowId(int borrowId) {
		this.borrowId = borrowId;
	}
	public String getCopyId() {
		return copyId;
	}
	public void setCopyId(String copyId) {
		this.copyId = copyId;
	}
	
	/**
	 * Hàm này thêm vào chi tiết của đăng ký mượn sách
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void addDetail() throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(ADD_DETAIL);
		ps.setInt(1, borrowId);
		ps.setString(2, copyId + "");
		ps.executeUpdate();
		closeDB();
	}

	/**
	 * Hàm này trả về chi tiết của một đăng ký mượn sách
	 * @param borrowId là id của đăng ký
	 * @return một ArrayList các chi tiết
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<BorrowDetail> getDetails(int borrowId) throws ClassNotFoundException, SQLException {
		ArrayList<BorrowDetail> details = new ArrayList<BorrowDetail>();
		connectDB();
		PreparedStatement ps = conn.prepareStatement(SEARCH_DETAILS);
		ps.setInt(1, borrowId);
		ResultSet rs = ps.executeQuery();
		if (rs != null) {
			while (rs.next()) {
				BorrowDetail detail = new BorrowDetail();
				detail.setBorrowId(rs.getInt("borrowID"));
				detail.setCopyId(rs.getString("copyId"));
				detail.setDetailId(rs.getInt("detailId"));
				details.add(detail);
			}
		}
		return details;
	}
}
