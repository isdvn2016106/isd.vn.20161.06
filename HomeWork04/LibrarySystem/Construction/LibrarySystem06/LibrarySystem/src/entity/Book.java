package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import book.controller.BookController;



/**
 * Hàm này quản lý Book trong cơ sở dữ liệu 
 * @author Sơn
 *
 */
public class Book extends DataAccessHelper{
	private static final String SEARCH_BOOK_BY_TITLE = "select * from `book` where title like ?";
	private static final String SEARCH_BOOK_BY_BOOKNUMBER = "select * from `book` where bookId like ?";
	private static final String ADD_BOOK = "INSERT INTO `book` (bookId, title, author, ISBN, classificationId, publisherId) VALUES (?,?,?,?,?,?)";
	private static final String GET_BOOKS = "select bookId, title, author, classificationId, publisherId from `book`";
	private static final String GET_BOOKS_BY_FILTER = "select bookId, title, author, classificationId, publisherId from `book` where ";
	private static final String COUNT_BOOK_BY_CLASSIFICATION = "select count(*) from `book` where classificationId = ?";
	private static final String FILTER_TITLE = "Title";
	private static final String FILTER_AUTHOR = "Author";
	private static final String FILTER_PUBLISHER = "Publisher";
	private static final String FILTER_CLASSIFICATION = "Classification";
	private static final String UPDATE_BOOK = "UPDATE `book` SET `title`= ?,`author`= ?,`ISBN`= ?,`classificationId` = ?,`publisherId`= ? WHERE bookID LIKE ?";
	private static final String DELETE_BOOK = "DELETE FROM `book` WHERE bookId LIKE ?";
	/* Attributes */
	private String bookNumber;
	private String title;
	private String author;
	private String ISBN;
	private int publisherId;
	private int classificationId;
	
	/* Constructors */
	
	/* Methods*/

	/**
	 * Hàm này dùng tìm sách theo mã sách
	 * @param bookNumber là mã sách
	 * @return kết quả tìm được hay không
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public boolean findByBookNumber(String bookNumber) throws SQLException, ClassNotFoundException{
		connectDB();
		PreparedStatement ps = conn.prepareStatement(SEARCH_BOOK_BY_BOOKNUMBER);
		ps.setString(1, bookNumber);
		ResultSet rs = ps.executeQuery();
		if(rs !=null && rs.next()){
            this.setAuthor(rs.getString("author"));
            this.setBookNumber(rs.getString("bookId"));
            this.setClassificationId(rs.getInt("classificationId"));
            this.setISBN(rs.getString("ISBN"));
            this.setPublisherId(rs.getInt("publisherId"));
            this.setTitle(rs.getString("title"));
            closeDB();
            return true;
        }
        closeDB();
        return false;
	}
	
	/**
	 * Hàm này tìm kiếm sach theo tiêu đề
	 * @param bookTitle là tiêu đề sách
	 * @return kết quả tìm được hay không
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public boolean findByBookTitle(String bookTitle) throws SQLException, ClassNotFoundException{
		connectDB();
		PreparedStatement ps = conn.prepareStatement(SEARCH_BOOK_BY_TITLE);
		ps.setString(1, bookTitle);
		ResultSet rs = ps.executeQuery();
		if(rs !=null && rs.next()){
            this.setAuthor(rs.getString("author"));
            this.setBookNumber(rs.getString("bookId"));
            this.setClassificationId(rs.getInt("classificationId"));
            this.setISBN(rs.getString("ISBN"));
            this.setPublisherId(rs.getInt("publisherId"));
            this.setTitle(rs.getString("title"));
            closeDB();
            return true;
        }
        closeDB();
        return false;
	}
	
	/**
	 * Hàm này dùng để thêm sách mới vào cơ sở dữ liệu
	 * @return kết quả thêm được hay không
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean addBook() throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(SEARCH_BOOK_BY_BOOKNUMBER);
		ps.setString(1, bookNumber);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			closeDB();
			return false;
		} else {
			ps = conn.prepareStatement(ADD_BOOK);
			ps.setString(1, bookNumber);
			ps.setString(2, title);
			ps.setString(3, author);
			ps.setString(4, ISBN);
			ps.setInt(5, classificationId);
			ps.setInt(6, publisherId);
			//System.out.println(ps.toString());
			ps.executeUpdate();
		}
		closeDB();
		return true;
	}
	

	/**
	 * Hàm này trả về toàn bộ sách trong cơ sở dữ liệu
	 * @return một ArrayList các sách
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<Book> findBooks() throws ClassNotFoundException, SQLException {
		ArrayList<Book> books = new ArrayList<Book>();
		String query = GET_BOOKS;
		connectDB();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Book item = new Book();
			item.setAuthor(rs.getString("author"));
			item.setBookNumber(rs.getString("bookId"));
			item.setTitle(rs.getString("title"));
			item.setClassificationId(rs.getInt("classificationId"));
			item.setPublisherId(rs.getInt("publisherId"));
			books.add(item);
		}
		closeDB();
		return books;
	}

	/**
	 * Hàm này trả về số lượng các sách theo một classification
	 * @param id là id của classification
	 * @return giá trị int là số lượng các sách
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int getBookCountByClassification(int id) throws ClassNotFoundException, SQLException {
		int count = 0;
		String query = COUNT_BOOK_BY_CLASSIFICATION;
		connectDB();
		PreparedStatement ps = conn.prepareStatement(query);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			count = rs.getInt(1);
		}
		closeDB();
		return count;
	}

	/**
	 * Hàm này trả về danh sách các sách được tìm kiếm theo đầu vào
	 * @param input là giá trị tìm kiếm
	 * @param filter bao gồm title, author, publisher, classification 
	 * @return một ArrayList các sách tìm kiếm được
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<Book> findBookByFilter(String input, String filter) throws ClassNotFoundException, SQLException {
		ArrayList<Book> books = new ArrayList<Book>();
		connectDB();
		String query = GET_BOOKS_BY_FILTER;
		switch (filter) {
		case FILTER_TITLE:
			query += "title like ?";
			break;
		case FILTER_AUTHOR:
			query += "author like ?";
			break;
		case FILTER_PUBLISHER:
			query += "publisherId = ?";
			break;
		case FILTER_CLASSIFICATION:
			query += "classificationId = ?";
			break;
		default:
			break;
		}
		PreparedStatement ps = conn.prepareStatement(query);
		if (filter.equals(FILTER_PUBLISHER)) {
			int id = BookController.getInst().findIdByName(SimpleQuery.PUBLISHER, input);
			ps.setInt(1, id);
		} else if (filter.equals(FILTER_CLASSIFICATION)) {
			int id = BookController.getInst().findIdByName(SimpleQuery.CLASSIFICATION, input);
			ps.setInt(1, id);
		} else ps.setString(1, input);
		ResultSet rs = ps.executeQuery();
		if (rs != null) {
			while (rs.next()) {
				Book item = new Book();
				item.setAuthor(rs.getString("author"));
				item.setBookNumber(rs.getString("bookId"));
				item.setTitle(rs.getString("title"));
				item.setClassificationId(rs.getInt("classificationId"));
				item.setPublisherId(rs.getInt("publisherId"));
				books.add(item);
			}
		}
		return books;
	}

	/**
	 * Hàm này dùng để cập nhật thông tin của sách 
	 * @return kết quả cập nhật
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean updateBook() throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(SEARCH_BOOK_BY_BOOKNUMBER);
		ps.setString(1, bookNumber);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			ps = conn.prepareStatement(UPDATE_BOOK);
			ps.setString(1, title);
			ps.setString(2, author);
			ps.setString(3, ISBN);
			ps.setInt(4, classificationId);
			ps.setInt(5, publisherId);
			ps.setString(6, bookNumber);
			ps.executeUpdate();
		} else {
			closeDB();
			return false;
		}
		closeDB();
		return true;
	}

	/**
	 * Hàm này dùng để xóa sách trong cơ sở dữ liệu
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void deleteBook() throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(DELETE_BOOK);
		ps.setString(1, bookNumber);
		ps.executeUpdate();
		closeDB();
	}

	/* Getters and Setters */
	
	public String getBookNumber() {
		return bookNumber;
	}

	public void setBookNumber(String bookNumber) {
		this.bookNumber = bookNumber;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getISBN() {
		return ISBN;
	}

	public void setISBN(String iSBN) {
		ISBN = iSBN;
	}

	public int getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(int publisherId) {
		this.publisherId = publisherId;
	}

	public int getClassificationId() {
		return classificationId;
	}

	public void setClassificationId(int classificationId) {
		this.classificationId = classificationId;
	}

	
}
