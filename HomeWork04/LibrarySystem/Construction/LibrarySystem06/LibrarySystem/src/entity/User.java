package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utilities.Constants;

/**
 * Class này quản lý User trong cơ sở dữ liệu
 * @author Blej
 *
 */
public class User extends DataAccessHelper{
	private static final String GET_USERNAME_BY_ID = "select `userName` from `userAccount` where userId = ?";
	private static final String GET_ID_BY_USERNAME = "select `userId` from `userAccount` where userName like ?";
	private static final String GET_LOGIN = "select * from `userAccount` where userName=? and password=?";
    private static final String SEARCH_USER_BY_USERNAME = "select * from `userAccount` where userName = ?";
    private static final String ADD_USER = "INSERT INTO `userAccount` (userName,password,fullName,email,gender,contact,role,borrowerType,studentId,studyPeriod) VALUES (?,?,?,?,?,?,?,?,?,?)";
    private static final String SEARCH_USER = "select fullName,email,gender,contact,role,borrowerType,studentId,studyPeriod from `userAccount` where";
    private static final String UPDATE_PASS = "update User set password = ?,requestToChange=? where email = ?";
    private static final String UPDATE_PROFILE ="update `userAccount` set password = ?,fullName=?,email=?,gender=?,contact=?,role=?,borrowerType=?,studentId=?,studyPeriod=? where userName = ?";
	private static final String GET_USERS_BY_ROLE = "SELECT * FROM `userAccount`";
	private static final String DELETE_BOOK = "DELETE FROM `userAccount` WHERE userName LIKE ?";
	private static final String SEARCH_USER_BY_ID = "select * from `userAccount` where userId = ?";
    private int userId;
    private String userName;
    private String password;
    private String email;
    private String fullName;
    private String gender;
    private String contact;
    private String role;
    private String borrowerType;
    private String studentId;
    private String studyPeriod;
	
    /* Methods */
    
    /**
     * Hàm này trả về role theo tên người dùng
     * @param userName là tên người dùng
     * @return một String là role của người dùng
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public String getRoleByUserame(String userName) throws SQLException, ClassNotFoundException{
    	if (findByUserName(userName)) {
    		return this.getRole();
    	} else return null;
    }
    
    /**
     * Hàm này trả về id của người dùng theo tên người dùng
     * @param username là tên người dùng
     * @return giá trị int là id người dùng
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public int getIDByUsername(String username) throws ClassNotFoundException, SQLException {
    	int id = 0;
    	connectDB();
    	PreparedStatement ps = conn.prepareStatement(GET_ID_BY_USERNAME);
    	ps.setString(1, username);
    	ResultSet rs = ps.executeQuery();
    	if (rs != null && rs.next()) {
    		id = rs.getInt("userid");
    	}
    	return id;
    }
    
    /**
     * Hàm này tìm kiếm người dùng theo tên
     * @param userName là tên người dùng
     * @return kết quả tìm kiếm
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public boolean findByUserName(String userName) throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(SEARCH_USER_BY_USERNAME);
        ps.setString(1, userName);
        ps.executeQuery();
        ResultSet rs = ps.executeQuery();
        if(rs !=null && rs.next()){
        	this.setUserName(userName);
            this.setFullName(rs.getString("fullName"));
            this.setEmail(rs.getString("email"));
            this.setContact(rs.getString("contact"));
            this.setPassword(rs.getString("password"));
            this.setGender(rs.getString("gender"));
            this.setRole(rs.getString("role"));
            this.setBorrowerType(rs.getString("borrowerType"));
            this.setStudentId(rs.getString("studentId"));
            this.setStudyPeriod(rs.getString("studyPeriod"));
            closeDB();
            return true;
        }
        closeDB();
        return false;
    }
    
    /**
     * Hàm này kiểm tra xem người dùng có được phép đăng nhập không
     * @param userName là tên người dùng
     * @param password là mật khẩu
     * @return là kết quả đăng nhập
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public int checkLogin(String userName, String password) throws SQLException, ClassNotFoundException {
        if(findByUserName(userName)){
            connectDB();
            PreparedStatement ps = conn.prepareStatement(GET_LOGIN);
            ps.setString(1, userName);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs != null && rs.next()) {                
            	closeDB();
                return Constants.LOGIN_SUCCESS; // đăng nhập thành công
            } else {
                closeDB();
                return Constants.LOGIN_WRONG_PASSWORD; // sai mật khẩu
            }
        }
        return Constants.LOGIN_WRONG_EMAIL; // email không tồn tại
    }
    
    /**
     * Hàm này dùng để thêm người dùng mới
     * @return kết quả thêm vao
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public boolean addUser() throws SQLException, ClassNotFoundException {
        connectDB();
        PreparedStatement ps;
        ps = conn.prepareStatement(SEARCH_USER_BY_USERNAME);
        ps.setString(1, email);
        ResultSet rs = ps.executeQuery();
        if (rs != null && rs.next()){
            closeDB();
            return false;
        }else{
            ps = conn.prepareStatement(ADD_USER);
            ps.setString(1, userName);
            ps.setString(2, password);
            ps.setString(3, fullName);
            ps.setString(4, email);
            ps.setString(5, gender);
            ps.setString(6, contact);
            ps.setString(7, role);
            ps.setString(8, borrowerType);
            ps.setString(9, studentId);
            ps.setString(10, studyPeriod);
//            System.out.println(ps);
            ps.executeUpdate();
        }
        closeDB();
        return true;
    }
    
    /**
     * Hàm này dùng để tìm kiếm người dùng theo các dữ liệu nhập vào
     * @param userName là tên người dùng
     * @param fullName là tên đầy đủ của người dùng
     * @param email là email của người dùng
     * @param role là role của người dùng
     * @return một ArratList các người dùng
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public ArrayList<User> searchUser(String userName, String fullName, String email, String role) throws SQLException, ClassNotFoundException{
        ArrayList<User> users = new ArrayList<User>();
        ResultSet rs;
        String queryForUserName = " userName like ?";
        String queryForFullName = " fullName like ?";
        String queryForEmail = " email like ?";
        String queryForRole = " role like ?";
        String query = SEARCH_USER;
        String queryAdd = "";
        if(!userName.isEmpty())
            queryAdd+=queryForUserName;
        if(!fullName.isEmpty()) {
        	if (!queryAdd.equals("")) queryAdd+=" and ";
            queryAdd+=queryForFullName;
        }
        if(!email.isEmpty()) {
        	if (!queryAdd.equals("")) queryAdd+=" and ";
        	queryAdd+=queryForEmail;
        }
        if(!role.isEmpty()) {
        	if (!queryAdd.equals("")) queryAdd+=" and ";
        	queryAdd+=queryForRole;
        }
        query+=queryAdd;
        connectDB();
        PreparedStatement ps = conn.prepareStatement(query);
        int count=1;
        if(!userName.isEmpty()){
            ps.setString(count, "%"+userName+"%");
            count++;
        }
        if(!fullName.isEmpty()){
            ps.setString(count, "%"+fullName+"%");
            count++;
        }
        if(!email.isEmpty()){
            ps.setString(count, "%"+email+"%");
            count++;
        }
        if(!role.isEmpty()) {
        	ps.setString(count, "%"+role+"%");
        }
        rs = ps.executeQuery();
        while(rs.next()){
            User user = new User();
            user.setFullName(rs.getString("firstName"));
            user.setEmail(rs.getString("email"));
            user.setGender(rs.getString("gender"));
            user.setRole(rs.getString("role"));
            user.setBorrowerType(rs.getString("borrowerType"));
            user.setStudentId(rs.getString("studentID"));
            user.setStudyPeriod(rs.getString("studyPeriod"));
            users.add(user);
        }
        closeDB();
        return users;
    }
    
    /**
     * Hàm này cập nhật thông tin người dùng
     * @param userName là tên người dùng
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public void updateProfile(String userName) throws SQLException, ClassNotFoundException{
        connectDB();
        PreparedStatement ps = conn.prepareStatement(UPDATE_PROFILE);
        ps.setString(1, password);
        ps.setString(2, fullName);
        ps.setString(3, email);
        ps.setString(4, gender);
        ps.setString(5, contact);
        ps.setString(6, role);
        ps.setString(7, borrowerType);
        ps.setString(8, studentId);
        ps.setString(9, studyPeriod);
        ps.setString(10, userName);
        ps.executeUpdate();
        closeDB();
    }
    
    /* Getters and Setters */
    public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getBorrowerType() {
		return borrowerType;
	}
	public void setBorrowerType(String borrowerType) {
		this.borrowerType = borrowerType;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getStudyPeriod() {
		return studyPeriod;
	}
	public void setStudyPeriod(String studyPeriod) {
		this.studyPeriod = studyPeriod;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Hàm này trả về tên người dùng theo id
	 * @param id là id của người dùng
	 * @return một String là tên người dùng
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public String getUsernameByID(int id) throws ClassNotFoundException, SQLException {
		String userName = "";
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_USERNAME_BY_ID);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			userName = rs.getString("userName");
		}
		return userName;
	}

	/**
	 * Hàm này trả về danh sách các người dùng theo role
	 * @param role 
	 * @return một ArrayList danh sách người dùng tìm kiếm được
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<User> getUsersByRole(String role) throws ClassNotFoundException, SQLException {
		ArrayList<User> users = new ArrayList<User>();
		connectDB();
		String query = GET_USERS_BY_ROLE;
		switch (role) {
		case "Admin":
			query += " WHERE role LIKE 'admin'";
			break;
		case "Librarian":
			query += " WHERE role LIKE 'librarian'";
			break;
		case "Borrower":
			query += " WHERE role LIKE 'borrower'";
			break;
		default:
			break;
		}
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		if (rs != null) {
			while (rs.next()) {
				User user = new User();
				user.setUserName(rs.getString("userName"));
				user.setRole(rs.getString("role"));
				user.setFullName(rs.getString("fullName"));
				user.setEmail(rs.getString("email"));
				users.add(user);
			}
		}
		return users;
	}

 
	/**
	 * Hàm này để tìm kiếm người dùng theo id
	 * @param id là id người dùng
	 * @return kết quả tìm kiếm
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean findById(int id) throws ClassNotFoundException, SQLException {
    	connectDB();
    	PreparedStatement ps = conn.prepareStatement(SEARCH_USER_BY_ID);
    	ps.setString(1, id + "");
    	ResultSet rs = ps.executeQuery();
    	if (rs != null && rs.next()) {
    		this.setUserName(rs.getString("userName"));
    		this.setFullName(rs.getString("fullName"));
    		this.setEmail(rs.getString("email"));
    		this.setContact(rs.getString("contact"));
    		this.setPassword(rs.getString("password"));
    		this.setGender(rs.getString("gender"));
    		this.setRole(rs.getString("role"));
    		this.setBorrowerType(rs.getString("borrowerType"));
    		this.setStudentId(rs.getString("studentId"));
    		this.setStudyPeriod(rs.getString("studyPeriod"));
            closeDB();
            return true; 
    	}
    	closeDB();
    	return false;
    }
}
