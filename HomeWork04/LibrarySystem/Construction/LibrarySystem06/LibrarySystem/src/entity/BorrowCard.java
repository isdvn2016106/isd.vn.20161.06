package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import user.controller.UserController;

/**
 * Class này quản lý BorrowCard trong cơ sở dữ liệu
 * @author Sơn
 *
 */
public class BorrowCard extends DataAccessHelper{
	private static final String GET_CARD_BY_USERNAME = "SELECT * FROM `borrowcard`, `userAccount` WHERE borrowcard.userId = userAccount.userId and userAccount.username like ?";
	private static final String GET_ACTIVATION_CODE = "SELECT * FROM `activationCode` WHERE activationCode LIKE ?";
	private static final String ADD_CARD = "INSERT INTO `borrowCard` (userId, expiredDate, activationCode) VALUES (?,?,?)";
	private static final String DELETE_CARD = "DELETE `borrowCard` FROM `borrowCard`,`userAccount` WHERE userAccount.userId = borrowCard.userID AND userAccount.userName LIKE ?";
	private static final String DELETE_CODE = "DELETE FROM `activationCode` WHERE activationCode LIKE ?";
	private static final String GET_ACTIVATION_CODE_BY_USERNAME = "SELECT activationCode FROM `borrowCard`, `userAccount` WHERE borrowCard.userId = userAccount.userID AND userAccount.userName LIKE ?";
	private static final String ADD_ACTIVATION_CODE = "INSERT INTO `activationCode`(activationCode) VALUES (?)";
	private static final String GET_CARD_BY_ID = "SELECT * FROM `borrowcard` WHERE cardId = ?";
	private static final String GET_CARDS = "SELECT * FROM `borrowCard`";
	private static final String UPDATE_CARD = "UPDATE `borrowCard` SET `expiredDate` = ? WHERE cardID = ?";
	private final String SEARCH_BORROW_CARD_BY_CARD_ID = "select * from `borrowcard` where cardId = ?";
	private int cardId;
	private int userId;
	private Timestamp expiredDate;
	private String activationCode;
	
	/**
	 * Hàm này kiểm tra xem thẻ của người dùng còn hạn sử dụng hay không
	 * @param userName tên người dùng
	 * @return kết quả 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean checkCard(String userName) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_CARD_BY_USERNAME);
		ps.setString(1, userName);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			Timestamp expiredDate = rs.getTimestamp("expiredDate");
			Timestamp currentDate = new Timestamp(new Date().getTime());
			if (currentDate.compareTo(expiredDate) > 0) 
				return false;
			else return true;
		} else {
			closeDB();
			return false;
		}
	}

	/**
	 * Hàm này tìm kiếm thẻ theo id
	 * @param cardId là id của thẻ
	 * @return kết quả tìm kiếm
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean findByCardId(int cardId) throws ClassNotFoundException, SQLException {
		connectDB();
    	PreparedStatement ps = conn.prepareStatement(SEARCH_BORROW_CARD_BY_CARD_ID);
    	ps.setString(1, cardId + "");
    	ResultSet rs = ps.executeQuery();
    	if (rs != null && rs.next()) {
    		this.setCardId(rs.getInt("cardId"));
    		this.setUserId(rs.getInt("userId"));
    		this.setExpiredDate(rs.getTimestamp("expiredDate"));
    		this.setActivationCode(rs.getString("activationCode"));
    		closeDB();
    		return true;
    	}
    	closeDB();
    	return false;
	}


	/**
	 * Hàm này kích hoạt tài khoản người dùng và thêm thẻ vào cơ sở dữ liệu
	 * @param userName là tên người dùng
	 * @param activationCode là mã kích hoạt thẻ
	 * @return kết quả kích hoạt
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean activate(String userName, String activationCode) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_ACTIVATION_CODE);
		ps.setString(1, activationCode);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			this.expiredDate = calculateExpiredDate();
			this.userId = UserController.getInst().getIDByUsername(userName);
			connectDB();
			ps = conn.prepareStatement(ADD_CARD);
			ps.setInt(1, userId);
			ps.setTimestamp(2, expiredDate);
			ps.setString(3, activationCode);
			ps.executeUpdate();
			ps = conn.prepareStatement(DELETE_CODE);
			ps.setString(1, activationCode);
			ps.executeUpdate();
			closeDB();
			return true;
		} else {
			closeDB();
			return false;
		}
		
	}

	private Timestamp calculateExpiredDate() {
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.DATE, 14);
		return new Timestamp(c.getTime().getTime());
	}

	/**
	 * Hàm này hủy kích hoạt tài khoản và xóa thẻ của người dùng
	 * @param userName là tên người dùng
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void deactivate(String userName) throws ClassNotFoundException, SQLException {
		String activationCode = getActivationCodeByUsername(userName);
		connectDB();
		PreparedStatement ps = conn.prepareStatement(DELETE_CARD);
		ps.setString(1, userName);
		ps.executeUpdate();
		if (!activationCode.equals("")) {
			ps = conn.prepareStatement(ADD_ACTIVATION_CODE);
			ps.setString(1, activationCode);
			ps.executeUpdate();
		}
		closeDB();
	}

	/**
	 * Hàm này trả về mã kích hoạt của một tài khoản
	 * @param userName là tên tài khoản
	 * @return một String là mã kích hoạt
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public String getActivationCodeByUsername(String userName) throws ClassNotFoundException, SQLException {
		String activationCode = "";
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_ACTIVATION_CODE_BY_USERNAME);
		ps.setString(1, userName);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			activationCode = rs.getString("activationCode");
		}
		closeDB();
		return activationCode;
	}

	/**
	 * Hàm này tìm kiếm thẻ theo mã thẻ
	 * @param id là mã thẻ
	 * @return kết quả tìm kiếm
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean getCardByID(int id) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_CARD_BY_ID);
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			this.activationCode = rs.getString("activationCode");
			this.cardId = id;
			this.expiredDate = rs.getTimestamp("expiredDate");
			this.userId = rs.getInt("userID");
			closeDB();
			return true;
		}
		closeDB();
		return false;
		
	}

	/**
	 * Hàm này trả về danh sách các thẻ
	 * @return một ArrayList các thẻ
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<BorrowCard> getCards() throws ClassNotFoundException, SQLException {
		ArrayList<BorrowCard> cards = new ArrayList<BorrowCard>();
		String query = GET_CARDS;
		connectDB();
		PreparedStatement ps = conn.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			BorrowCard item = new BorrowCard();
			item.setActivationCode(rs.getString("activationCode"));
			item.setCardId(rs.getInt("cardId"));
			item.setExpiredDate(rs.getTimestamp("expiredDate"));
			item.setUserId(rs.getInt("userId"));
			cards.add(item);
		}
		closeDB();
		return cards;
	}

	/**
	 * Hàm này dùng để cập nhật thông tin thẻ
	 * @return kết quả cập nhật
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean updateCard() throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_CARD_BY_ID);
		ps.setInt(1, cardId);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			ps = conn.prepareStatement(UPDATE_CARD);
			ps.setTimestamp(1, expiredDate);
			ps.setInt(2, cardId);
			ps.executeUpdate();
		} else {
			closeDB();
			return false;
		}
		closeDB();
		return true;
	}

	/**
	 * Hàm này tìm kiếm thẻ theo tên tài khoản
	 * @param userName là tên tài khoản
	 * @return kết quả tìm kiếm
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean getCardByUsername(String userName) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_CARD_BY_USERNAME);
		ps.setString(1, userName);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			this.activationCode = rs.getString("activationCode");
			this.cardId = rs.getInt("cardID");
			this.expiredDate = rs.getTimestamp("expiredDate");
			this.userId = rs.getInt("userID");
			closeDB();
			return true;
		}
		closeDB();
		return false;
	}
	
	public int getCardId() {
		return cardId;
	}
	public void setCardId(int cardId) {
		this.cardId = cardId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public Timestamp getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(Timestamp expiredDate) {
		this.expiredDate = expiredDate;
	}
	public String getActivationCode() {
		return activationCode;
	}
	public void setActivationCode(String activationCode) {
		this.activationCode = activationCode;
	}
}
