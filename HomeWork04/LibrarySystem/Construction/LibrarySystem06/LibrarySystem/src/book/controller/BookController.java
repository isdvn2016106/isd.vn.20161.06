package book.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import entity.Book;
import entity.BookCopy;
import entity.SimpleQuery;

/**
 * Class này điều khiển các chức năng liên quan đến sách
 * @author Sơn
 *
 */
public class BookController {
	/* Attributes */
	
	/* Constructors*/
	private BookController() {
		
	}
	private static BookController inst;
	public static BookController getInst() {
		if (inst == null)
			inst = new BookController();
		return inst;
	}
	/* Methods*/
	/**
	 * Hàm này dùng để tìm kiếm sách theo BookNumber
	 * @param bookNumber
	 * @return sách tìm được hoặc null nếu không tìm được sách theo BookNumber
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public Book findBookByBookNumber(String bookNumber) throws ClassNotFoundException, SQLException {
		Book book = new Book();
		if (book.findByBookNumber(bookNumber)) 
			return book;
		return null;
	}
	
	/**
	 * Hàm này để tìm sách theo tiêu đề
	 * @param bookTitle
	 * @return sách tìm được hoặc null 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public Book findBookByTitle(String bookTitle) throws ClassNotFoundException, SQLException {
		Book book = new Book();
		if (book.findByBookTitle(bookTitle))
			return book;
		return null;
	}
	
	/**
	 * Hàm này dùng để thêm sách vào bảng Book trong cơ sở dữ liệu
	 * @param book
	 * @return kết quả thêm được hay không
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean addBook(Book book) throws ClassNotFoundException, SQLException {
		return book.addBook();
	}
	/**
	 * Hàm này trả về toàn bộ sách trong cơ sở dữ liệu
	 * @return một ArrayList các sách
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<Book> getBooks() throws ClassNotFoundException, SQLException {
		Book book = new Book();
		ArrayList<Book> books = book.findBooks();
		return books;
	}
	
	/**
	 * Hàm này trả về id của Publisher hoặc Classification theo tên
	 * @param tableName là tên bảng Publisher hay Classification
	 * @param name là tên của id cần trả về
	 * @return giá trị int là id 
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int findIdByName(int tableName, String name) throws ClassNotFoundException, SQLException {
		SimpleQuery sq = new SimpleQuery();
		return sq.findIdByName(tableName, name);
	}
	
	/**
	 * Hàm này trả về tên của Publisher hoặc Classification theo id
	 * @param tableName là tên bảng Publisher hay Classification
	 * @param id là id của tên cần trả về
	 * @return giá trị String là tên
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public String findNameById(int tableName, int id) throws ClassNotFoundException, SQLException {
		SimpleQuery sq = new SimpleQuery();
		return sq.findNamebyId(tableName, id);
	}
	
	/**
	 * Hàm này trả về mã Classification theo tên
	 * @param name là tên Classification
	 * @return giá trị String là mã Classification
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public String findClassificationCodeByName(String name) throws ClassNotFoundException, SQLException {
		SimpleQuery sq = new SimpleQuery();
		return sq.findClassificationCodebyName(name);
	}
	
	/**
	 * Hàm này trả về số sách trong cơ sở dữ liệu theo Classification nhập vào
	 * @param id là id của classification
	 * @return giá trị int là số sách
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int getBookCountByClassification(int id) throws ClassNotFoundException, SQLException {
		Book book = new Book();
		return book.getBookCountByClassification(id);
	}
	
	/**
	 * Hàm này trả về bản sao của mã bản sao được nhập vào
	 * @param id là mã bản sao
	 * @return một đối tượng BookCopy là bản sao tìm được hoặc null
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public BookCopy findCopyById(String id) throws ClassNotFoundException, SQLException {
		BookCopy copy = new BookCopy();
		if (copy.findCopyByID(id))
			return copy;
		return null;
	}
	/**
	 * Hàm này dùng để thêm bản sao mới vào cơ sở dữ liệu
	 * @param copy là bản sao cần thêm
	 * @return kết quả thêm vào
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean addCopy(BookCopy copy) throws ClassNotFoundException, SQLException {
		return copy.addCopy();
	}
	/**
	 * Hàm này trả về số bản sao hiện có của sách
	 * @param bookNumber là mã sách
	 * @return giá trị int là số bản sao
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int getCopyCountByBookNumber(String bookNumber) throws ClassNotFoundException, SQLException {
		BookCopy copy = new BookCopy();
		return copy.getCopyCountByBookNumber(bookNumber);
	}
	/**
	 * Hàm này trả về số bản sao có thể cho mượn hiện có của sách
	 * @param bookNumber là mã sách
	 * @return giá trị int là số bản sao
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int getBorrowableCopyCount(String bookNumber) throws ClassNotFoundException, SQLException {
		BookCopy copy = new BookCopy();
		return copy.getBorrowableCopyCount(bookNumber);
	}
	/**
	 * Hàm này trả về danh sách các sách được tìm kiếm theo đầu vào
	 * @param input là giá trị đầu vào
	 * @param filter bao gồm title, publisher, classification hoặc author
	 * @return một ArrayList các sách tìm được
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<Book> getBooksByFilter(String input, String filter) throws ClassNotFoundException, SQLException {
		Book book = new Book();
		ArrayList<Book> books = book.findBookByFilter(input, filter);
		return books;
	}
	/**
	 * Hàm này dùng để cập nhật thông tin cho sách 
	 * @param book là sách cần cập nhật thông tin
	 * @return kết quả cập nhật được hay không
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean updateBook(Book book) throws ClassNotFoundException, SQLException {
		return book.updateBook();
	}
	/**
	 * Hàm này dùng để xóa sách trong cơ sở dữ liệu
	 * @param book là sách cần xóa
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void deleteBook(Book book) throws ClassNotFoundException, SQLException {
		BookCopy copy = new BookCopy();
		copy.setBookId(book.getBookNumber());
		copy.deleteCopy();
		book.deleteBook();
	}
	/**
	 * Hàm này trả về giá tiền của sách
	 * @param bookNumber là sách cần tìm giá tiền
	 * @return giá trị int là giá tiền của sách
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int getPriceByBookNumber(String bookNumber) throws ClassNotFoundException, SQLException {
		BookCopy copy = new BookCopy();
		return copy.getPriceByBookNumber(bookNumber);
	}
	/**
	 * Hàm này trả về một bản sao theo mã sách
	 * @param bookNumber là mã sách
	 * @return một bản sao theo mã sách
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public BookCopy getACopyByBookNumber(String bookNumber) throws ClassNotFoundException, SQLException {
		BookCopy copy = new BookCopy();
		if (copy.getACopyByBookNumber(bookNumber)) return copy;
		return null;
	}
	public void makeCopyAvailable(String copyId) throws ClassNotFoundException, SQLException {
		BookCopy copy = new BookCopy();
		copy.makeAvailable(copyId);
	}
}