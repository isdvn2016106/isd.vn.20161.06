package book.boundary;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.EtchedBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import book.controller.BookController;
import entity.Book;
import entity.SimpleQuery;

import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;

/**
 * Class này là lớp biên của chức năng quản lý sách
 * @author Sơn
 *
 */
public class ManageBookForm extends JPanel{
	private String userName;
	private JTextField txtSearch;
	private JTable table;
	private JLabel lblAuthor;
	private JLabel lblTitle;
	private JLabel lblPublisher;
	private JLabel lblBookNumber;
	private JLabel lblISBN;
	private JLabel lblClassification;
	private JLabel lblStatus;
	private JLabel lblCopiesLeft;
	private JComboBox cbbFilter;
	private Book selectedBook;
	private JButton btnDelete;
	private JButton btnUpdate;

	/**
	 * Create the panel.
	 */
	public ManageBookForm(String userName) {
		this.userName = userName;
		initComponents();
	}

	private void initComponents() {
		this.setSize(800, 600);
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		
		JButton btnAddBook = new JButton("Add Book");
		btnAddBook.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAddBookActionPerformed(e);
			}
		});
		btnAddBook.setSize(90, 30);
		
		JButton btnAddCopy = new JButton("Add Copy");
		btnAddCopy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAddCopyActionPerformed();
			}
		});
		btnAddCopy.setSize(90, 30);
		
		JPanel panel_1 = new JPanel();
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(btnAddBook)
									.addGap(18)
									.addComponent(btnAddCopy)
									.addGap(301))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(panel, GroupLayout.DEFAULT_SIZE, 782, Short.MAX_VALUE)
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addGap(10))
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 782, Short.MAX_VALUE)
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnAddBook)
						.addComponent(btnAddCopy))
					.addGap(24))
		);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 0, 429, 488);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(447, 0, 325, 488);
		panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		btnUpdate = new JButton("Update Book");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateActionPerformed();
			}
		});
		btnUpdate.setVisible(false);
		
		btnDelete = new JButton("Delete Book");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteActionPerformed();
			}
		});
		btnDelete.setVisible(false);
		
		JLabel label = new JLabel("Book Detail");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setFont(new Font("Dialog", Font.BOLD, 14));
		
		JLabel label_1 = new JLabel("Author:");
		
		lblAuthor = new JLabel("");
		
		JLabel label_3 = new JLabel("Publisher:");
		
		lblPublisher = new JLabel("");
		
		JLabel label_5 = new JLabel("Classification:");
		
		lblClassification = new JLabel("");
		
		JLabel label_7 = new JLabel("ISBN:");
		
		lblISBN = new JLabel("");
		
		lblStatus = new JLabel("");
		
		lblCopiesLeft = new JLabel("");
		
		JLabel label_15 = new JLabel("Book Number:");
		
		JLabel label_16 = new JLabel("Title:");
		
		lblTitle = new JLabel("");
		
		lblBookNumber = new JLabel("");
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(label_15, Alignment.TRAILING)
						.addComponent(label_16, Alignment.TRAILING)
						.addComponent(label_1, Alignment.TRAILING)
						.addComponent(label_3, Alignment.TRAILING)
						.addComponent(label_5, Alignment.TRAILING)
						.addComponent(label_7, Alignment.TRAILING))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING)
						.addComponent(lblAuthor, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblPublisher, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblClassification, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblISBN, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblTitle, GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
						.addComponent(lblBookNumber, GroupLayout.DEFAULT_SIZE, 252, Short.MAX_VALUE)
						.addComponent(lblStatus, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCopiesLeft, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 252, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
				.addGroup(gl_panel_2.createSequentialGroup()
					.addComponent(label, GroupLayout.PREFERRED_SIZE, 321, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap(70, Short.MAX_VALUE)
					.addComponent(btnUpdate)
					.addGap(18)
					.addComponent(btnDelete)
					.addGap(75))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addComponent(label)
					.addGap(18)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_15)
						.addComponent(lblBookNumber))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_16)
						.addComponent(lblTitle))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_1)
						.addComponent(lblAuthor))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_3)
						.addComponent(lblPublisher))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_5)
						.addComponent(lblClassification))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_7)
						.addComponent(lblISBN))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblStatus)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCopiesLeft)
					.addPreferredGap(ComponentPlacement.RELATED, 248, Short.MAX_VALUE)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnUpdate)
						.addComponent(btnDelete))
					.addContainerGap())
		);
		panel_2.setLayout(gl_panel_2);
		panel_1.setLayout(null);
		
		table = new JTable();
		tableInit();
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		panel_1.add(scrollPane);
		panel_1.add(panel_2);
		ListSelectionModel rowSelectionModel = table.getSelectionModel();
		rowSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		rowSelectionModel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				rowValueChanged(e);
			}
		});
		
		JLabel lblSearchBooks = new JLabel("Search Books:");
		
		txtSearch = new JTextField();
		txtSearch.setColumns(10);
		
		JButton btnSe = new JButton("Search");
		btnSe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSearchActionPerformed();
			}
		});
		
		cbbFilter = new JComboBox();
		cbbFilter.setModel(new DefaultComboBoxModel(new String[] {"Title", "Author", "Publisher", "Classification"}));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSearchBooks)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtSearch, GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(cbbFilter, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnSe)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSearchBooks)
						.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSe)
						.addComponent(cbbFilter, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(15, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		setLayout(groupLayout);
	}


	private void btnDeleteActionPerformed() {
		int input = JOptionPane.showConfirmDialog(this, "Are you sure?", "Delete Book", JOptionPane.OK_CANCEL_OPTION);
		if (input == JOptionPane.OK_OPTION) {
			try {
				deleteBook(selectedBook);
				JOptionPane.showMessageDialog(this, "Success!", "Delete success!", JOptionPane.INFORMATION_MESSAGE);
				genTableContent(table, BookController.getInst().getBooks());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void deleteBook(Book book) {
		try {
			BookController.getInst().deleteBook(book);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void btnUpdateActionPerformed() {
		UpdateBook ub = new UpdateBook(selectedBook, this);
		ub.setVisible(true);
		ub.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private void tableInit() {
		try {
			genTableContent(table,BookController.getInst().getBooks());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void btnSearchActionPerformed() {
		try {
			String filter = cbbFilter.getSelectedItem().toString();
			String input = txtSearch.getText();
			ArrayList<Book> books = BookController.getInst().getBooksByFilter(input, filter);
			genTableContent(table, books);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void genTableContent(JTable table, ArrayList<Book> books){
        try {
            Vector<String> row,colunm;
            int count =1;
            DefaultTableModel model = new DefaultTableModel();
            String[] colunmNames = {"Book Number", "Title", "Author", "Publisher", "Classification"};
            colunm = new Vector<String>();
            int numberColumn;
            numberColumn = colunmNames.length;
            for(int i =0; i<numberColumn;i++){
                colunm.add(colunmNames[i]);
            }   
            model.setColumnIdentifiers(colunm);
            for(Book book : books){
                row = new Vector<String>();
                row.add(book.getBookNumber());
                row.add(book.getTitle());
                row.add(book.getAuthor());
                String publisher = BookController.getInst().findNameById(SimpleQuery.PUBLISHER, book.getPublisherId());
                String classification = BookController.getInst().findNameById(SimpleQuery.CLASSIFICATION, book.getClassificationId());
                row.add(publisher);
                row.add(classification);
                //row.add(Integer.toString(book.getPublisherId()));
                //row.add(Integer.toString(book.getClassificationId()));
                count++;
                model.addRow(row);
            }   table.setModel(model);
            table.setVisible(true);
        } catch (SQLException ex) {
        	ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(this, "Occur error durring run: "+ex.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
        }
    }   

	private void btnAddBookActionPerformed(ActionEvent e) {
		AddBook addBook = new AddBook(this);
		addBook.setVisible(true);
		addBook.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
	private void btnAddCopyActionPerformed() {
		AddCopy addCopy = new AddCopy();
		addCopy.setVisible(true);
		addCopy.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private void rowValueChanged(ListSelectionEvent e) {
		try {
			int rowNumber = table.getSelectedRow();
			if (rowNumber >= 0) {
				String bookNumber = (String) table.getValueAt(table.getSelectedRow(), 0);
				Book book = BookController.getInst().findBookByBookNumber(bookNumber);
				lblBookNumber.setText(bookNumber);
				lblTitle.setText(book.getTitle());
				lblAuthor.setText(book.getAuthor());
				lblISBN.setText(book.getISBN());
				lblPublisher.setText(BookController.getInst().findNameById(SimpleQuery.PUBLISHER, book.getPublisherId()));
				lblClassification.setText(BookController.getInst().findNameById(SimpleQuery.CLASSIFICATION, book.getClassificationId()));
				setSelectedBook(book);
				btnUpdate.setVisible(true);
				btnDelete.setVisible(true);
			} else return;
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	public JTable getTable() {
		return table;
	}

	public Book getSelectedBook() {
		return selectedBook;
	}

	public void setSelectedBook(Book selectedBook) {
		this.selectedBook = selectedBook;
	}
	
}
