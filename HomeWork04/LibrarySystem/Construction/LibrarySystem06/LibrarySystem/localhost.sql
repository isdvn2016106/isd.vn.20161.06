-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 18, 2016 at 07:01 PM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mydb`
--
CREATE DATABASE `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mydb`;

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `bookId` varchar(10) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `ISBN` varchar(45) DEFAULT NULL,
  `classificationId` int(11) NOT NULL,
  `publisherId` int(11) NOT NULL,
  PRIMARY KEY (`bookId`,`classificationId`,`publisherId`),
  KEY `fk_Book_Classification1_idx` (`classificationId`),
  KEY `fk_Book_Publisher1_idx` (`publisherId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`bookId`, `title`, `author`, `ISBN`, `classificationId`, `publisherId`) VALUES
('NV0001', 'The Definition of Insanity', 'Vass Montenegro', 'ISBN 000-111-222', 1, 2),
('NV0002', 'Heroes Never Die...For A Price', 'Angela Zeigler', 'ISBN 6695-8795', 1, 2),
('NV0003', 'Just Do It', 'Shia LaBoeuf', 'ISBN 123123-456', 1, 2),
('RB0001', 'Rails Tutorial', 'Michael Hartl', 'ISBN 123-456789', 1, 1),
('RB0002', 'How to Build an AK-47', 'Kalashnikov', 'ISBN 0123-474747', 1, 1),
('RB0003', 'Code Clean', 'Unknown', 'ISBN 0000000-111', 2, 1),
('RB0004', 'Head First Java', 'HFTeam', 'ISBN 11111-2223', 2, 1),
('RB0005', 'Head First Android', 'HFTeam', 'ISBN 2222656-56', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `bookcopy`
--

CREATE TABLE IF NOT EXISTS `bookcopy` (
  `copyId` int(11) NOT NULL AUTO_INCREMENT,
  `copyType` varchar(15) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `condition` varchar(45) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `bookId` varchar(10) NOT NULL,
  PRIMARY KEY (`copyId`,`bookId`),
  KEY `fk_BookCopy_Book1_idx` (`bookId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `borrowcard`
--

CREATE TABLE IF NOT EXISTS `borrowcard` (
  `cardId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `expiredDate` datetime DEFAULT NULL,
  `activationCode` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`cardId`,`userId`),
  KEY `fk_BorrowCard_UserAccount_idx` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `borrowdetail`
--

CREATE TABLE IF NOT EXISTS `borrowdetail` (
  `detailId` int(11) NOT NULL AUTO_INCREMENT,
  `borrowId` int(11) NOT NULL,
  `copyId` int(11) NOT NULL,
  PRIMARY KEY (`detailId`,`borrowId`,`copyId`),
  KEY `fk_BorrowDetail_BorrowRegistration1_idx` (`borrowId`),
  KEY `fk_BorrowDetail_BookCopy1_idx` (`copyId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `borrowregistration`
--

CREATE TABLE IF NOT EXISTS `borrowregistration` (
  `borrowId` int(11) NOT NULL AUTO_INCREMENT,
  `registeredDate` datetime DEFAULT NULL,
  `lentDate` datetime DEFAULT NULL,
  `expectedReturnDate` datetime DEFAULT NULL,
  `returnedDate` datetime DEFAULT NULL,
  `compensation` int(11) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `cardId` int(11) NOT NULL,
  PRIMARY KEY (`borrowId`,`cardId`),
  KEY `fk_BorrowRegistration_BorrowCard1_idx` (`cardId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `classification`
--

CREATE TABLE IF NOT EXISTS `classification` (
  `classificationId` int(11) NOT NULL,
  `classificationName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`classificationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classification`
--

INSERT INTO `classification` (`classificationId`, `classificationName`) VALUES
(1, 'Novel'),
(2, 'Reference Book');

-- --------------------------------------------------------

--
-- Table structure for table `publisher`
--

CREATE TABLE IF NOT EXISTS `publisher` (
  `publisherId` int(11) NOT NULL,
  `publisherName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`publisherId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `publisher`
--

INSERT INTO `publisher` (`publisherId`, `publisherName`) VALUES
(1, 'NXB Giáo dục'),
(2, 'NXB Văn Học');

-- --------------------------------------------------------

--
-- Table structure for table `useraccount`
--

CREATE TABLE IF NOT EXISTS `useraccount` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `fullName` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `contact` varchar(100) DEFAULT NULL,
  `role` varchar(10) DEFAULT NULL,
  `borrowerType` varchar(15) DEFAULT NULL,
  `studentId` varchar(10) DEFAULT NULL,
  `studyPeriod` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `useraccount`
--

INSERT INTO `useraccount` (`userId`, `username`, `password`, `fullName`, `email`, `gender`, `contact`, `role`, `borrowerType`, `studentId`, `studyPeriod`) VALUES
(1, 'admin', '0e7517141fb53f21ee439b355b5a1d0a', 'Admin', 'admin@admin.com', 'Male', '0123456789', 'Admin', 'Non-Student', '', ''),
(2, 'lib1', 'bc3f3191f70122183dd5f171bd8e67e1', 'Librarian 1', 'lib1@librarian.com', 'Male', '0123789456', 'Librarian', 'Non-Student', '', ''),
(3, 'lib2', 'a7e1b477dd31232d5f7db3f74f6f4b87', 'Librarian 2', 'lib2@librarian.com', 'Female', '0987123456', 'Librarian', 'Non-Student', '', '');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `fk_Book_Classification1` FOREIGN KEY (`classificationId`) REFERENCES `classification` (`classificationId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Book_Publisher1` FOREIGN KEY (`publisherId`) REFERENCES `publisher` (`publisherId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bookcopy`
--
ALTER TABLE `bookcopy`
  ADD CONSTRAINT `fk_BookCopy_Book1` FOREIGN KEY (`bookId`) REFERENCES `book` (`bookId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `borrowcard`
--
ALTER TABLE `borrowcard`
  ADD CONSTRAINT `fk_BorrowCard_UserAccount` FOREIGN KEY (`userId`) REFERENCES `useraccount` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `borrowdetail`
--
ALTER TABLE `borrowdetail`
  ADD CONSTRAINT `fk_BorrowDetail_BookCopy1` FOREIGN KEY (`copyId`) REFERENCES `bookcopy` (`copyId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_BorrowDetail_BorrowRegistration1` FOREIGN KEY (`borrowId`) REFERENCES `borrowregistration` (`borrowId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `borrowregistration`
--
ALTER TABLE `borrowregistration`
  ADD CONSTRAINT `fk_BorrowRegistration_BorrowCard1` FOREIGN KEY (`cardId`) REFERENCES `borrowcard` (`cardId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
