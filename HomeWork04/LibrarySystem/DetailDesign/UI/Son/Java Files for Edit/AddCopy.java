package UIDesign;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.border.EtchedBorder;

public class AddCopy {

	private JFrame frmAddBookOr;
	private JTextField txtXx;
	private JTextField txtXx_1;
	private JTextField txtDefinitionOfInsanity;
	private JTextField txtVaasMotenegro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddCopy window = new AddCopy();
					window.frmAddBookOr.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddCopy() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAddBookOr = new JFrame();
		frmAddBookOr.setTitle("Add Book or Copy");
		frmAddBookOr.setBounds(100, 100, 452, 314);
		frmAddBookOr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAddBookOr.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(10, 11, 414, 44);
		frmAddBookOr.getContentPane().add(panel);
		panel.setLayout(null);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Add Copy", "Add Book"}));
		comboBox.setBounds(10, 11, 72, 20);
		panel.add(comboBox);
		
		JLabel lblBookNumber = new JLabel("Book Number:");
		lblBookNumber.setBounds(92, 14, 72, 14);
		panel.add(lblBookNumber);
		
		txtXx = new JTextField();
		txtXx.setText("XX9999");
		txtXx.setBounds(165, 11, 140, 20);
		panel.add(txtXx);
		txtXx.setColumns(10);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.setBounds(315, 10, 89, 23);
		panel.add(btnSearch);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_1.setBounds(10, 54, 414, 210);
		frmAddBookOr.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblBookNumber_1 = new JLabel("Book Number:");
		lblBookNumber_1.setBounds(10, 11, 73, 14);
		panel_1.add(lblBookNumber_1);
		
		JLabel lblNumberOfNew = new JLabel("Number of New Copies:");
		lblNumberOfNew.setBounds(10, 95, 122, 14);
		panel_1.add(lblNumberOfNew);
		
		JLabel lblTypeOfCopy = new JLabel("Type of Copy:");
		lblTypeOfCopy.setBounds(10, 120, 73, 14);
		panel_1.add(lblTypeOfCopy);
		
		JLabel lblPriceOfCopy = new JLabel("Price of Copy:");
		lblPriceOfCopy.setBounds(10, 145, 73, 14);
		panel_1.add(lblPriceOfCopy);
		
		txtXx_1 = new JTextField();
		txtXx_1.setText("XX9999");
		txtXx_1.setBounds(137, 8, 267, 20);
		panel_1.add(txtXx_1);
		txtXx_1.setColumns(10);
		txtXx_1.setEditable(false);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerNumberModel(new Integer(5), null, null, new Integer(1)));
		spinner.setBounds(137, 92, 267, 20);
		panel_1.add(spinner);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Reference", "Borrowable"}));
		comboBox_1.setBounds(137, 117, 267, 20);
		panel_1.add(comboBox_1);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerNumberModel(new Integer(120000), new Integer(0), null, new Integer(1000)));
		spinner_1.setBounds(137, 142, 267, 20);
		panel_1.add(spinner_1);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(163, 173, 89, 23);
		panel_1.add(btnSubmit);
		
		JLabel lblTitle = new JLabel("Title:");
		lblTitle.setBounds(10, 39, 73, 14);
		panel_1.add(lblTitle);
		
		txtDefinitionOfInsanity = new JTextField();
		txtDefinitionOfInsanity.setText("Definition of Insanity");
		txtDefinitionOfInsanity.setEditable(false);
		txtDefinitionOfInsanity.setColumns(10);
		txtDefinitionOfInsanity.setBounds(137, 36, 267, 20);
		panel_1.add(txtDefinitionOfInsanity);
		
		JLabel lblAuthor = new JLabel("Author");
		lblAuthor.setBounds(10, 67, 73, 14);
		panel_1.add(lblAuthor);
		
		txtVaasMotenegro = new JTextField();
		txtVaasMotenegro.setText("Vaas Motenegro");
		txtVaasMotenegro.setEditable(false);
		txtVaasMotenegro.setColumns(10);
		txtVaasMotenegro.setBounds(137, 64, 267, 20);
		panel_1.add(txtVaasMotenegro);
	}
}
