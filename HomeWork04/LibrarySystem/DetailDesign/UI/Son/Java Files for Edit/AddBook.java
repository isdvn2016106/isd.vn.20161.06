package UIDesign;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.border.EtchedBorder;

public class AddBook {

	private JFrame frmAddBookOr;
	private JTextField txtXx;
	private JTextField txtDefinitionOfInsanity;
	private JTextField txtUbisoft;
	private JTextField txtVaasMotenegro;
	private JTextField txtIsbn;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddBook window = new AddBook();
					window.frmAddBookOr.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddBook() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAddBookOr = new JFrame();
		frmAddBookOr.setTitle("Add Book or Copy");
		frmAddBookOr.setBounds(100, 100, 450, 300);
		frmAddBookOr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAddBookOr.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(10, 11, 414, 44);
		frmAddBookOr.getContentPane().add(panel);
		panel.setLayout(null);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Add Book", "Add Copy"}));
		comboBox.setBounds(10, 11, 81, 20);
		panel.add(comboBox);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_1.setBounds(10, 55, 414, 195);
		frmAddBookOr.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblBookNumber = new JLabel("Book Number:");
		lblBookNumber.setBounds(10, 14, 67, 14);
		panel_1.add(lblBookNumber);
		
		txtXx = new JTextField();
		txtXx.setText("XX9999");
		txtXx.setBounds(79, 11, 325, 20);
		panel_1.add(txtXx);
		txtXx.setColumns(10);
		
		JLabel lblTitle = new JLabel("Title:");
		lblTitle.setBounds(10, 42, 67, 14);
		panel_1.add(lblTitle);
		
		txtDefinitionOfInsanity = new JTextField();
		txtDefinitionOfInsanity.setText("Definition of Insanity");
		txtDefinitionOfInsanity.setColumns(10);
		txtDefinitionOfInsanity.setBounds(79, 39, 325, 20);
		panel_1.add(txtDefinitionOfInsanity);
		
		JLabel lblPublisher = new JLabel("Publisher:");
		lblPublisher.setBounds(10, 70, 67, 14);
		panel_1.add(lblPublisher);
		
		txtUbisoft = new JTextField();
		txtUbisoft.setText("Ubisoft");
		txtUbisoft.setColumns(10);
		txtUbisoft.setBounds(79, 67, 325, 20);
		panel_1.add(txtUbisoft);
		
		JLabel lblAuthor = new JLabel("Author:");
		lblAuthor.setBounds(10, 98, 67, 14);
		panel_1.add(lblAuthor);
		
		txtVaasMotenegro = new JTextField();
		txtVaasMotenegro.setText("Vaas Motenegro");
		txtVaasMotenegro.setColumns(10);
		txtVaasMotenegro.setBounds(79, 95, 325, 20);
		panel_1.add(txtVaasMotenegro);
		
		JLabel lblIsbn = new JLabel("ISBN");
		lblIsbn.setBounds(10, 126, 67, 14);
		panel_1.add(lblIsbn);
		
		txtIsbn = new JTextField();
		txtIsbn.setText("ISBN 123456789-0");
		txtIsbn.setColumns(10);
		txtIsbn.setBounds(79, 123, 325, 20);
		panel_1.add(txtIsbn);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(162, 161, 89, 23);
		panel_1.add(btnSubmit);
	}
}
