package UIDesign;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JSplitPane;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.border.EtchedBorder;

public class RegisterForBorrowingBook {

	private JFrame frmRegisterForBorrowing;
	private JTextField textField;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterForBorrowingBook window = new RegisterForBorrowingBook();
					window.frmRegisterForBorrowing.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public RegisterForBorrowingBook() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRegisterForBorrowing = new JFrame();
		frmRegisterForBorrowing.setTitle("Register for Borrowing Books");
		frmRegisterForBorrowing.setBounds(100, 100, 800, 600);
		frmRegisterForBorrowing.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRegisterForBorrowing.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 764, 44);
		frmRegisterForBorrowing.getContentPane().add(panel);
		panel.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(86, 11, 458, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.setBounds(665, 10, 89, 23);
		panel.add(btnSearch);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Title", "Author", "Publisher", "Classification"}));
		comboBox.setBounds(554, 11, 101, 20);
		panel.add(comboBox);
		
		JLabel lblSearchBooks = new JLabel("Search books:");
		lblSearchBooks.setBounds(10, 14, 77, 14);
		panel.add(lblSearchBooks);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(10, 52, 764, 498);
		frmRegisterForBorrowing.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setToolTipText("");
		scrollPane.setBounds(10, 11, 379, 441);
		panel_1.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, "Definition of Insanity", "Vass Motenegro", "Ubisoft", "Novel"},
				{Boolean.TRUE, "Metro 2033", "Russian Guy", "4A", "Novel"},
				{null, "Syndicate", "Evie Frye", "Ubisoft", "Novel"},
				{Boolean.TRUE, "Becoz I'm Batman", "Batman", "DC Comics", "Novel"},
				{null, "Blyatiful", "Boris", "YT", "Novel"},
				{null, "Cyka Girlfriend", "Boris", "YT", "Novel"},
				{null, "Metro 2034", "Russian Guy", "4A", "Novel"},
				{null, "Metro 2035", "Russian Guy", "4A", null},
			},
			new String[] {
				"Selected", "Title", "Author", "Publisher", "Classification"
			}
		) {
			Class[] columnTypes = new Class[] {
				Boolean.class, String.class, String.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			boolean[] columnEditables = new boolean[] {
				true, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		table.getColumnModel().getColumn(0).setResizable(false);
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(399, 11, 355, 441);
		panel_1.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblIsbn_1 = new JLabel("ISBN:");
		lblIsbn_1.setBounds(10, 126, 67, 14);
		panel_2.add(lblIsbn_1);
		
		JLabel label_1 = new JLabel("Author:");
		label_1.setBounds(10, 98, 67, 14);
		panel_2.add(label_1);
		
		JLabel label_2 = new JLabel("Publisher:");
		label_2.setBounds(10, 70, 67, 14);
		panel_2.add(label_2);
		
		JLabel label_3 = new JLabel("Title:");
		label_3.setBounds(10, 42, 67, 14);
		panel_2.add(label_3);
		
		JLabel label_4 = new JLabel("Book Number:");
		label_4.setBounds(10, 14, 67, 14);
		panel_2.add(label_4);
		
		JLabel lblXx = new JLabel("XX9999");
		lblXx.setBounds(87, 14, 258, 14);
		panel_2.add(lblXx);
		
		JLabel lblDefinitionOfInsanity = new JLabel("Definition of Insanity");
		lblDefinitionOfInsanity.setBounds(87, 42, 258, 14);
		panel_2.add(lblDefinitionOfInsanity);
		
		JLabel lblUbisoft = new JLabel("Ubisoft");
		lblUbisoft.setBounds(87, 70, 258, 14);
		panel_2.add(lblUbisoft);
		
		JLabel lblVaasMontenegro = new JLabel("Vaas Montenegro");
		lblVaasMontenegro.setBounds(87, 98, 258, 14);
		panel_2.add(lblVaasMontenegro);
		
		JLabel lblIsbn = new JLabel("ISBN 123456789-0");
		lblIsbn.setBounds(87, 126, 258, 14);
		panel_2.add(lblIsbn);
		
		JLabel lblClassification = new JLabel("Classification:");
		lblClassification.setBounds(10, 151, 67, 14);
		panel_2.add(lblClassification);
		
		JLabel lblNovel = new JLabel("Novel");
		lblNovel.setBounds(87, 151, 258, 14);
		panel_2.add(lblNovel);
		
		JLabel lblPrice = new JLabel("Price:");
		lblPrice.setBounds(10, 176, 67, 14);
		panel_2.add(lblPrice);
		
		JLabel lblVnd = new JLabel("120,000 VND");
		lblVnd.setBounds(87, 176, 258, 14);
		panel_2.add(lblVnd);
		
		JLabel lblStatus = new JLabel("Status:");
		lblStatus.setBounds(10, 201, 67, 14);
		panel_2.add(lblStatus);
		
		JLabel lblBorrowable = new JLabel("Borrowable");
		lblBorrowable.setBounds(87, 201, 258, 14);
		panel_2.add(lblBorrowable);
		
		JLabel lblCopiesLeft = new JLabel("Copies Left: ");
		lblCopiesLeft.setBounds(10, 226, 67, 14);
		panel_2.add(lblCopiesLeft);
		
		JLabel label_5 = new JLabel("3");
		label_5.setBounds(87, 226, 258, 14);
		panel_2.add(label_5);
		
		JLabel lblNumberOfBooks = new JLabel("Number of books selected: 2/5");
		lblNumberOfBooks.setBounds(10, 463, 153, 24);
		panel_1.add(lblNumberOfBooks);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(344, 463, 89, 23);
		panel_1.add(btnSubmit);
	}
}
