package user.boundary;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.EtchedBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import book.controller.BookController;
import entity.Book;
import entity.BorrowCard;
import entity.SimpleQuery;
import user.controller.CardController;
import user.controller.UserController;

import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;

/**
 * Class này là lớp biên của chức năng quản lý thẻ
 * @author Sơn
 *
 */
public class ManageCardForm extends JPanel{
	private String userName;
	private JTextField txtSearch;
	private JTable table;
	private JLabel lblExpiredDate;
	private JLabel lblUsername;
	private JLabel lblActivationCode;
	private JLabel lblCardID;
	private JButton btnDelete;
	private JButton btnUpdate;
	private BorrowCard selectedCard;

	/**
	 * Create the panel.
	 */
	public ManageCardForm(String userName) {
		this.userName = userName;
		initComponents();
	}

	private void initComponents() {
		this.setSize(800, 600);
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		
		JPanel panel_1 = new JPanel();
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel, GroupLayout.DEFAULT_SIZE, 782, Short.MAX_VALUE)
							.addGap(10))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 782, Short.MAX_VALUE)
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
					.addGap(53))
		);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 0, 283, 488);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(303, 0, 469, 488);
		panel_2.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		
		btnUpdate = new JButton("Update Card");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateActionPerformed();
			}
		});
		
		btnDelete = new JButton("Deactivate Card");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteActionPerformed();
			}
		});
		
		JLabel lblCardDetail = new JLabel("Card Detail");
		lblCardDetail.setHorizontalAlignment(SwingConstants.CENTER);
		lblCardDetail.setFont(new Font("Dialog", Font.BOLD, 14));
		
		JLabel lbl3 = new JLabel("Expired Date:");
		
		lblExpiredDate = new JLabel("");
		
		JLabel lbl4 = new JLabel("Activation Code:");
		
		lblActivationCode = new JLabel("");
		
		JLabel lbl1 = new JLabel("Card ID:");
		
		JLabel lbl2 = new JLabel("UserName:");
		
		lblUsername = new JLabel("");
		
		lblCardID = new JLabel("");
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addComponent(lblCardDetail, GroupLayout.DEFAULT_SIZE, 465, Short.MAX_VALUE)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(49)
					.addComponent(lbl1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCardID, GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(37)
					.addComponent(lbl2)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblUsername, GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(24)
					.addComponent(lbl3)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblExpiredDate, GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addComponent(lbl4)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblActivationCode, GroupLayout.DEFAULT_SIZE, 359, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(gl_panel_2.createSequentialGroup()
					.addGap(112)
					.addComponent(btnUpdate)
					.addGap(51)
					.addComponent(btnDelete)
					.addContainerGap(98, Short.MAX_VALUE))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblCardDetail)
					.addGap(18)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lbl1)
						.addComponent(lblCardID))
					.addGap(18)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lbl2)
						.addComponent(lblUsername))
					.addGap(18)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lbl3)
						.addComponent(lblExpiredDate))
					.addGap(18)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(lbl4)
						.addComponent(lblActivationCode))
					.addPreferredGap(ComponentPlacement.RELATED, 292, Short.MAX_VALUE)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnUpdate)
						.addComponent(btnDelete))
					.addContainerGap())
		);
		panel_2.setLayout(gl_panel_2);
		panel_1.setLayout(null);
		
		table = new JTable();
		tableInit();
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		panel_1.add(scrollPane);
		panel_1.add(panel_2);
		ListSelectionModel rowSelectionModel = table.getSelectionModel();
		rowSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		rowSelectionModel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				rowValueChanged(e);
			}
		});
		
		JLabel lbl0 = new JLabel("Username:");
		
		txtSearch = new JTextField();
		txtSearch.setColumns(10);
		
		JButton btnSe = new JButton("Search");
		btnSe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSearchActionPerformed();
			}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lbl0)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtSearch, GroupLayout.DEFAULT_SIZE, 635, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnSe)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lbl0)
						.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSe))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		setLayout(groupLayout);
	}


	private void btnDeleteActionPerformed() {
		int input = JOptionPane.showConfirmDialog(this, "Are you sure?", "Deactivate Card", JOptionPane.OK_CANCEL_OPTION);
		if (input == JOptionPane.OK_OPTION) {
			try {
				deactivateCard(selectedCard);
				JOptionPane.showMessageDialog(this, "Success!", "Delete success!", JOptionPane.INFORMATION_MESSAGE);
				genTableContent(table, CardController.getInst().getCards());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private void deactivateCard(BorrowCard selectedCard) {
		try {
			CardController.getInst().deactivateCard(UserController.getInst().getUsernameByID(selectedCard.getUserId()));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void btnUpdateActionPerformed() {
		UpdateCard uc = new UpdateCard(getSelectedCard(), this);
		uc.setVisible(true);
		uc.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private void tableInit() {
		try {
			genTableContent(table, CardController.getInst().getCards());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void btnSearchActionPerformed() {
		try {
			String userName = txtSearch.getText();
			BorrowCard card = CardController.getInst().getCardByUserName(userName);
			ArrayList<BorrowCard> cards = new ArrayList<BorrowCard>();
			if (card != null) cards.add(card);
			genTableContent(table, cards);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void genTableContent(JTable table, ArrayList<BorrowCard> cards){
        try {
            Vector<String> row,colunm;
            DefaultTableModel model = new DefaultTableModel();
            String[] colunmNames = {"Card ID", "Username"};
            colunm = new Vector<String>();
            int numberColumn;
            numberColumn = colunmNames.length;
            for(int i =0; i<numberColumn;i++){
                colunm.add(colunmNames[i]);
            }   
            model.setColumnIdentifiers(colunm);
            for (BorrowCard card : cards) {
            	row = new Vector<String>();
            	row.add(Integer.toString(card.getCardId()));
            	row.add(UserController.getInst().getUsernameByID(card.getUserId()));
            	model.addRow(row);
            }
            table.setModel(model);
            table.setVisible(true);
        } catch (SQLException ex) {
        	ex.printStackTrace();
            JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
        } catch (ClassNotFoundException ex) {
            JOptionPane.showMessageDialog(this, "Occur error durring run: "+ex.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
        }
    }   

	private void rowValueChanged(ListSelectionEvent e) {
		try {
			int rowNumber = table.getSelectedRow();
			if (rowNumber >= 0) {
				String cardID = (String) table.getValueAt(table.getSelectedRow(), 0);
				BorrowCard card = CardController.getInst().getCardByCardID(Integer.parseInt(cardID));
				lblCardID.setText(cardID);
				lblUsername.setText(UserController.getInst().getUsernameByID(card.getUserId()));
				lblExpiredDate.setText(card.getExpiredDate().toString());
				lblActivationCode.setText(card.getActivationCode());
				setSelectedCard(card);
			} else return;
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	public JTable getTable() {
		return table;
	}

	public BorrowCard getSelectedCard() {
		return selectedCard;
	}

	public void setSelectedCard(BorrowCard selectedCard) {
		this.selectedCard = selectedCard;
	}
	
}
