package user.boundary;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.Color;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import entity.Function;
import user.controller.UserController;
import utilities.FunctionMap;

/**
 * Class này là lớp biên gen ra menu và màn hình chính
 * @author Sơn
 *
 */
public class MainForm {
	
	private String userName;
	private JFrame frame;
	private JPanel panel;

	/**
	 * Create the application.
	 */
	public MainForm(String userName) {
		this.userName = userName;
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 650);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Library System - Username: " + userName);
		frame.setVisible(true);
		panel = new JPanel();
		frame.setContentPane(panel);
		//System.out.println(panel.getPreferredSize().toString());
		try {
			String role = UserController.getInst().getRoleByUserName(userName);
			initialize(role);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(String role) {
			
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(Color.LIGHT_GRAY);
		frame.setJMenuBar(menuBar);
		
		JMenu mnAccount = new JMenu("Tài Khoản");
		menuBar.add(mnAccount);
		
		JMenuItem mnLogOut = new JMenuItem("Đăng xuất");
		mnAccount.add(mnLogOut);
		mnLogOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegisterLoginForm rlForm = new RegisterLoginForm();
                rlForm.setVisible(true);
                frame.dispose();
			}
		});
		
		JMenu mnFunction = new JMenu("Chức năng");
		menuBar.add(mnFunction);
		
		ArrayList<Function> functions = FunctionMap.getInst().getFunctionMap().get(role);
		JMenuItem menuItem;
		for (Function function : functions) {
			menuItem = new JMenuItem(function.getFunctionName());
			mnFunction.add(menuItem);
			menuItem.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					JPanel jpanel = null;
					try {
						Class classToLoad = Class.forName(function.getFunctionClassName());
						Class[] cArg = new Class[1];
						cArg[0] = String.class;
						
						//jpanel = (JPanel) Class.forName(function.getFunctionClassName()).newInstance();
						jpanel = (JPanel) classToLoad.getDeclaredConstructor(cArg).newInstance(userName);
					} catch (InstantiationException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					} catch (SecurityException e) {
						e.printStackTrace();
					}
					panel.removeAll();
					panel.repaint();
					panel.validate();
					panel.add(jpanel);
					jpanel.setVisible(true);
					panel.repaint();
					panel.validate();
				}
			});
		}
	}

}
