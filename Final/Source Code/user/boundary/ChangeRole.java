package user.boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import user.controller.UserController;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.jws.soap.SOAPBinding.Use;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

/**
 * Class này là lớp biên của chức năng thay đổi role
 * @author Sơn
 *
 */
public class ChangeRole extends JFrame {

	private JPanel contentPane;
	private String username;
	private JComboBox cbbRole;
	private ManageUserForm manageUserForm;

	public ChangeRole(String username, String role, ManageUserForm manageUserForm) {
		this.username = username;
		this.manageUserForm = manageUserForm;
		setTitle("Change Role");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 136);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblRole = new JLabel("Role:");
		
		cbbRole = new JComboBox();
		cbbRole.setModel(new DefaultComboBoxModel(new String[] {"Admin", "Librarian", "Borrower"}));
		cbbRole.setSelectedItem(role);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSubmitActionPerformed();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(158, Short.MAX_VALUE)
					.addComponent(lblRole)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(cbbRole, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
					.addGap(155))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(178)
					.addComponent(btnSubmit)
					.addContainerGap(181, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblRole)
						.addComponent(cbbRole, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnSubmit)
					.addContainerGap(22, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}

	private void btnSubmitActionPerformed() {
		try {
			UserController.getInst().updateRole(username, cbbRole.getSelectedItem().toString());
			manageUserForm.genTableContent(manageUserForm.getTable(), UserController.getInst().getUsersByRole("All"));
			JOptionPane.showMessageDialog(this, "Success!");
			this.dispose();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
