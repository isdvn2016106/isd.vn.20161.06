package user.boundary;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.DefaultComboBoxModel;
import javax.swing.border.EtchedBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import book.controller.BookController;
import entity.Book;
import entity.SimpleQuery;
import entity.User;
import user.controller.UserController;

import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;

/**
 * Class này là lớp biên của chức năng quản lý người dùng
 * @author Sơn
 *
 */
public class ManageUserForm extends JPanel{
	private String userName;
	private JTable table;
	private User selectedUser;
	private JComboBox cbbRole;
	private JButton btnChangeRole;
	private JButton btnDeleteAccount;

	/**
	 * Create the panel.
	 */
	public ManageUserForm(String userName) {
		this.userName = userName;
		initComponents();
	}

	private void initComponents() {
		this.setSize(800, 600);
		
		JPanel panel = new JPanel();
		
		JScrollPane scrollPane = new JScrollPane();
		
		btnChangeRole = new JButton("Change Role");
		btnChangeRole.setEnabled(false);
		btnChangeRole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnChangeRoleActionPerformed();
			}
		});
		
		btnDeleteAccount = new JButton("Delete Account");
		btnDeleteAccount.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteAccountActionPerformed();
			}
		});
		btnDeleteAccount.setEnabled(false);
		
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(panel, GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(91)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 609, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(274)
							.addComponent(btnChangeRole, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
							.addGap(30)
							.addComponent(btnDeleteAccount)))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 449, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnChangeRole)
						.addComponent(btnDeleteAccount))
					.addContainerGap(52, Short.MAX_VALUE))
		);
		
		table = new JTable();
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		ListSelectionModel rowSelectionModel = table.getSelectionModel();
		rowSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		rowSelectionModel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				rowValueChanged(e);
			}
		});
		tableInit();
		
		JLabel lblRole = new JLabel("Role:");
		
		cbbRole = new JComboBox();
		cbbRole.setModel(new DefaultComboBoxModel(new String[] {"All", "Admin", "Librarian", "Borrower"}));
		cbbRole.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cbbRoleActionPerformed();
			}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(81)
					.addComponent(lblRole)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(cbbRole, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(583, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblRole)
						.addComponent(cbbRole, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		tableInit();
		setLayout(groupLayout);
	}

	private void btnDeleteAccountActionPerformed() {
		int input = JOptionPane.showConfirmDialog(this, "Are you sure?", "Delete Account", JOptionPane.OK_CANCEL_OPTION);
		if (input == JOptionPane.OK_OPTION) {
			deleteAccount(selectedUser);
			JOptionPane.showMessageDialog(this, "Success");
		}
	}

	private void btnChangeRoleActionPerformed() {
		if (selectedUser.getRole().equals("Admin")) {
			JOptionPane.showMessageDialog(this, "You cannot change role of an admin!", "Error!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		ChangeRole cg = new ChangeRole(selectedUser.getUserName(), selectedUser.getRole(), this);
		cg.setVisible(true);
		cg.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	private void cbbRoleActionPerformed() {
		try {
			genTableContent(table, UserController.getInst().getUsersByRole(cbbRole.getSelectedItem().toString()));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void deleteAccount(User user) {
	}

	private void tableInit() {
		try {
			ArrayList<User> users;
			users = UserController.getInst().getUsersByRole("All");
			genTableContent(table, users);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void genTableContent(JTable table, ArrayList<User> users){
            Vector<String> row,colunm;
            int count =1;
            DefaultTableModel model = new DefaultTableModel();
            String[] colunmNames = {"Username", "Role", "Full Name", "Email"};
            colunm = new Vector<String>();
            int numberColumn;
            numberColumn = colunmNames.length;
            for(int i =0; i<numberColumn;i++){
                colunm.add(colunmNames[i]);
            }   
            model.setColumnIdentifiers(colunm);
            for (User user : users) {
            	row = new Vector<String>();
            	row.add(user.getUserName());
            	row.add(user.getRole());
            	row.add(user.getFullName());
            	row.add(user.getEmail());
            	count++;
            	model.addRow(row);
            }
            table.setModel(model);
            table.setVisible(true);
    }   

	private void rowValueChanged(ListSelectionEvent e) {
		try {
			int rowNumber = table.getSelectedRow();
			if (rowNumber >= 0) {
				String username = (String) table.getValueAt(table.getSelectedRow(), 0);
				User user = UserController.getInst().findUserByUserName(username);
				setSelectedUser(user);
				btnChangeRole.setEnabled(true);
			}
		} catch (SQLException e2) {
			e2.printStackTrace();
		} catch (ClassNotFoundException e2) {
			e2.printStackTrace();
		}
	}

	public JTable getTable() {
		return table;
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}
}
