package user.controller;

import java.sql.SQLException;
import java.util.ArrayList;

import entity.BorrowCard;

/**
 * Class này là lớp điều khiển của thẻ
 * @author Sơn
 *
 */
public class CardController {
	private CardController() {
		
	}
	private static CardController inst;
	public static CardController getInst() {
		if (inst == null)
			inst = new CardController();
		return inst;
	}
	
	/**
	 * Hàm này kiểm tra tình trạng thẻ của người dùng
	 * @param userName là tên người dùng
	 * @return tình trạng thẻ người dùng
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean checkCard(String userName) throws ClassNotFoundException, SQLException {
		BorrowCard card = new BorrowCard();
		return card.checkCard(userName);
	}

	/**
	 * Hàm này dùng để kích hoạt tài khoản và thêm thẻ vào cơ sở dữ liệu
	 * @param userName là tên tài khoản
	 * @param activationCode là mã kích hoạt
	 * @return kết quả kích hoạt
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean activateCard(String userName, String activationCode) throws ClassNotFoundException, SQLException {
		BorrowCard card = new BorrowCard();
		return card.activate(userName, activationCode);
	}

	/**
	 * Hàm này hủy kích hoạt tài khoản và xóa thẻ tương ứng
	 * @param userName là tên tài khoản
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void deactivateCard(String userName) throws ClassNotFoundException, SQLException {
		BorrowCard card = new BorrowCard();
		card.deactivate(userName);
	}

	/**
	 * Hàm này tìm kiếm thẻ theo id 
	 * @param id là id thẻ cần tìm
	 * @return thẻ cẩn tìm
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public BorrowCard getCardByCardID(int id) throws ClassNotFoundException, SQLException {
		BorrowCard card = new BorrowCard();
		if (card.getCardByID(id))
			return card;
		return null;
	}

	/**
	 * Hàm này trả về danh sách các thẻ 
	 * @return một ArrayList các thẻ
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<BorrowCard> getCards() throws ClassNotFoundException, SQLException {
		BorrowCard card = new BorrowCard();
		ArrayList<BorrowCard> cards = card.getCards();
		return cards;
	}

	/**
	 * Hàm này cập nhật thông tin của một thẻ
	 * @param card là thẻ cần cập nhật thông tin
	 * @return kết quả cập nhật
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean updateCard(BorrowCard card) throws ClassNotFoundException, SQLException {
		return card.updateCard();
	}

	/**
	 * Hàm này tìm kiếm thẻ theo tên tài khoản
	 * @param userName là tên tài khoản
	 * @return thẻ tương ứng
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public BorrowCard getCardByUserName(String userName) throws ClassNotFoundException, SQLException {
		BorrowCard card = new BorrowCard();
		if (card.getCardByUsername(userName))
			return card;
		return null;
	}
}
