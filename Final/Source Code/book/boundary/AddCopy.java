package book.boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.border.EtchedBorder;

import book.controller.BookController;
import entity.Book;
import entity.BookCopy;

import javax.swing.JComboBox;
import javax.swing.JSpinner;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SpinnerNumberModel;

/**
 * Class này là lớp biên để thêm bản sao mới
 * @author Sơn
 *
 */
public class AddCopy extends JFrame {

	private JPanel contentPane;
	private JTextField txtSearch;
	private JTextField txtBookNum;
	private JTextField txtTitle;
	private JTextField txtAuthor;
	private JSpinner txtPrice;
	private ArrayList<String> errorMsgs;
	private JComboBox cbbCopyType;
	private JSpinner txtNumOfCopy;
	private JButton btnSearch;
	private JButton btnSubmit;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddCopy frame = new AddCopy();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AddCopy() {
		setTitle("Add Copy");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 319);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(10, 11, 414, 42);
		contentPane.add(panel);
		
		JLabel lblBookNumber = new JLabel("Book Number:");
		
		txtSearch = new JTextField();
		txtSearch.setColumns(10);
		
		btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSearchActionPerformed();
			}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblBookNumber)
					.addGap(18)
					.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, 237, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnSearch)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblBookNumber)
						.addComponent(txtSearch, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSearch))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_1.setBounds(10, 64, 414, 211);
		contentPane.add(panel_1);
		
		JLabel lblBookNumber_1 = new JLabel("Book Number: ");
		
		txtBookNum = new JTextField();
		txtBookNum.setEditable(false);
		txtBookNum.setColumns(10);
		
		JLabel lblTitle = new JLabel("Title:");
		
		txtTitle = new JTextField();
		txtTitle.setEditable(false);
		txtTitle.setColumns(10);
		
		JLabel lblAuthor = new JLabel("Author:");
		
		txtAuthor = new JTextField();
		txtAuthor.setEditable(false);
		txtAuthor.setColumns(10);
		
		JLabel lblNumOfCopy = new JLabel("Number of Copies");
		
		JLabel lblCopyType = new JLabel("Copy Type:");
		
		JLabel lblPrice = new JLabel("Price:");
		
		btnSubmit = new JButton("Submit");
		btnSubmit.setEnabled(false);
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSubmitActionPerformed();
			}
		});
		
		cbbCopyType = new JComboBox();
		cbbCopyType.setModel(new DefaultComboBoxModel(new String[] {"Borrowable", "Reference"}));
		
		txtPrice = new JSpinner();
		txtPrice.setModel(new SpinnerNumberModel(new Integer(10000), new Integer(10000), null, new Integer(1000)));
		
		txtNumOfCopy = new JSpinner();
		txtNumOfCopy.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel_1.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_panel_1.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblPrice)
								.addComponent(lblCopyType)
								.addComponent(lblNumOfCopy)
								.addComponent(lblAuthor)
								.addComponent(lblTitle)
								.addComponent(lblBookNumber_1))
							.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel_1.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addComponent(txtTitle, GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
										.addComponent(txtBookNum, GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
										.addComponent(txtAuthor, GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)))
								.addGroup(gl_panel_1.createSequentialGroup()
									.addGap(4)
									.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
										.addComponent(txtPrice, GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
										.addComponent(cbbCopyType, 0, 301, Short.MAX_VALUE)
										.addComponent(txtNumOfCopy, GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)))))
						.addGroup(gl_panel_1.createSequentialGroup()
							.addGap(173)
							.addComponent(btnSubmit)))
					.addContainerGap())
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblBookNumber_1)
						.addComponent(txtBookNum, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTitle)
						.addComponent(txtTitle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAuthor)
						.addComponent(txtAuthor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNumOfCopy)
						.addComponent(txtNumOfCopy, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCopyType)
						.addComponent(cbbCopyType, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_1.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPrice)
						.addComponent(txtPrice, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnSubmit)
					.addContainerGap(12, Short.MAX_VALUE))
		);
		panel_1.setLayout(gl_panel_1);
	}

	private void btnSubmitActionPerformed() {
        addCopy();
	}

	private void addCopy() {		
		try {
			int numOfCopies = (int) txtNumOfCopy.getValue();
			for (int i = 0; i<numOfCopies;i++) {
				BookCopy copy = new BookCopy();
				copy.setBookId(txtBookNum.getText());
				copy.setCopyType(cbbCopyType.getSelectedItem().toString());
				copy.setPrice((int) txtPrice.getValue());
				if (cbbCopyType.getSelectedItem().toString().equals("Borrowable")) {
					copy.setStatus("Available");
				} else copy.setStatus("Unavailable");
				int count = BookController.getInst().getCopyCountByBookNumber(txtBookNum.getText())+1;
				copy.setCopyId(txtBookNum.getText()+count);
				if (!BookController.getInst().addCopy(copy)) {		
					JOptionPane.showMessageDialog(this, "Error adding copies!", "Error!", JOptionPane.ERROR_MESSAGE);
				}
			}
			JOptionPane.showMessageDialog(this, "Success!", "Add copies success", JOptionPane.INFORMATION_MESSAGE);
			this.dispose();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void btnSearchActionPerformed() {
		try {
			String bookNumber = txtSearch.getText();
			Book book = BookController.getInst().findBookByBookNumber(bookNumber);
			if (book != null) {
				txtBookNum.setText(book.getBookNumber());
				txtTitle.setText(book.getTitle());
				txtAuthor.setText(book.getAuthor());
				txtBookNum.repaint();
				txtTitle.repaint();
				txtAuthor.repaint();
				btnSubmit.setEnabled(true);
			} else {
				JOptionPane.showMessageDialog(this, "No book found!", "Error!", JOptionPane.ERROR_MESSAGE);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
