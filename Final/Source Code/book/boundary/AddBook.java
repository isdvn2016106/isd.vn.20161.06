package book.boundary;


import java.awt.HeadlessException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import book.controller.BookController;
import entity.Book;
import entity.SimpleQuery;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

/**
 * Class này là lớp biên để thêm sách mới
 * @author Sơn
 *
 */
public class AddBook extends JFrame {

	private ManageBookForm manageBookForm;
	private JPanel contentPane;
	private JTextField txtBookNum;
	private JLabel lblTitle;
	private JTextField txtTitle;
	private JLabel lblAuthor;
	private JTextField txtAuthor;
	private JLabel lblPublisher;
	private JComboBox cbbPublisher;
	private JLabel lblIsbn;
	private JTextField txtISBN;
	private JLabel lblClassification;
	private JComboBox cbbClassification;
	private JButton btnSubmit;
	private ArrayList<String> errorMsgs;

	/**
	 * Create the frame.
	 */
	public AddBook(ManageBookForm manageBookForm) {
		this.manageBookForm = manageBookForm;
		setTitle("Add Book");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblBookNumber = new JLabel("Book Number:");
		
		txtBookNum = new JTextField();
		txtBookNum.setColumns(10);
		
		lblTitle = new JLabel("Title:");
		
		txtTitle = new JTextField();
		txtTitle.setColumns(10);
		
		lblAuthor = new JLabel("Author:");
		
		txtAuthor = new JTextField();
		txtAuthor.setColumns(10);
		
		lblPublisher = new JLabel("Publisher:");
		
		cbbPublisher = new JComboBox();
		addItemtoCbb(cbbPublisher, SimpleQuery.PUBLISHER, "publisherName");
		
		lblIsbn = new JLabel("ISBN:");
		
		txtISBN = new JTextField();
		txtISBN.setColumns(10);
		
		lblClassification = new JLabel("Classification:");
		
		cbbClassification = new JComboBox();
		addItemtoCbb(cbbClassification, SimpleQuery.CLASSIFICATION, "classificationName");
		cbbClassification.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				cbbClassificationItemChanged(e);
			}
		});
		
		btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSubmitActionPerformed();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblBookNumber, Alignment.TRAILING)
								.addComponent(lblTitle, Alignment.TRAILING)
								.addComponent(lblAuthor, Alignment.TRAILING)
								.addComponent(lblPublisher, Alignment.TRAILING)
								.addComponent(lblIsbn, Alignment.TRAILING)
								.addComponent(lblClassification, Alignment.TRAILING))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(txtTitle, GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
								.addComponent(txtBookNum, GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
								.addComponent(txtAuthor, GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
								.addComponent(cbbPublisher, 0, 333, Short.MAX_VALUE)
								.addComponent(txtISBN, GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
								.addComponent(cbbClassification, 0, 334, Short.MAX_VALUE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(176)
							.addComponent(btnSubmit)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblBookNumber)
						.addComponent(txtBookNum, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTitle)
						.addComponent(txtTitle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAuthor)
						.addComponent(txtAuthor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPublisher)
						.addComponent(cbbPublisher, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblIsbn)
						.addComponent(txtISBN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblClassification)
						.addComponent(cbbClassification, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(btnSubmit)
					.addContainerGap(24, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
		errorMsgs = new ArrayList<String>();
	}

	private void cbbClassificationItemChanged(ItemEvent e) {
		try {
			String classificationName = (String) e.getItem();
			String classificationCode = BookController.getInst().findClassificationCodeByName(classificationName);
			int classificationId = BookController.getInst().findIdByName(SimpleQuery.CLASSIFICATION, classificationName);
			int count = BookController.getInst().getBookCountByClassification(classificationId)+1;
			String bookNumber = classificationCode+count;
			int index = 2;
			while (bookNumber.length() < 6) {
				bookNumber = bookNumber.substring(0, index++) + "0" + count;
			}
			txtBookNum.setText(bookNumber);
			txtBookNum.repaint();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}

	private void addItemtoCbb(JComboBox<String> comboBox, int tableName, String column) {
		ArrayList<String> items = new ArrayList<String>();
		SimpleQuery sq = new SimpleQuery();
		try {
			items = sq.getAll(tableName, column);
			for (String item : items) {
				comboBox.addItem(item);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void btnSubmitActionPerformed() {
		errorMsgs.clear();
        try {
			if(checkInput()) {
				addBook();
				manageBookForm.genTableContent(manageBookForm.getTable(), BookController.getInst().getBooks());
			}
			else {
				String error = "";
				for (String msg : errorMsgs) {
					error+=msg;
					error+="\n";
				}
				JOptionPane.showMessageDialog(null, error, "Error!", JOptionPane.ERROR_MESSAGE);
			}
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private boolean checkInput() throws ClassNotFoundException, SQLException {
		boolean hasError = false;
		String bookNumber = txtBookNum.getText();
		if (bookNumber.equals("")) {
			errorMsgs.add("Book Number can't be blank!");
			hasError = true;
		} else if (BookController.getInst().findBookByBookNumber(bookNumber)!=null) {
			errorMsgs.add("Book is already exist!");
			hasError = true;
		}
		if (txtTitle.getText().equals("")) {
			errorMsgs.add("Title can't be blank!");
			hasError = true;
		}
		if (txtAuthor.getText().equals("")) {
			errorMsgs.add("Author can't be blank!");
			hasError = true;
		}
		if (txtISBN.getText().equals("")) {
			errorMsgs.add("ISBN can't be blank!");
			hasError = true;
		}
		return !hasError;
	}

	private void addBook() {
		try {
			Book book = new Book();
			int publisherId = BookController.getInst().findIdByName(SimpleQuery.PUBLISHER, (String) cbbPublisher.getSelectedItem());
			int classificationId = BookController.getInst().findIdByName(SimpleQuery.CLASSIFICATION, (String) cbbClassification.getSelectedItem());
			book.setPublisherId(publisherId);
			book.setClassificationId(classificationId);
			book.setBookNumber(txtBookNum.getText());
			book.setTitle(txtTitle.getText());
			book.setAuthor(txtAuthor.getText());
			book.setISBN(txtISBN.getText());
			if (BookController.getInst().addBook(book)) {
				int input = JOptionPane.showOptionDialog(null, "Success! Do you want to add another book?", "Add book success", JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE, null, null, null);
				if (input == JOptionPane.OK_OPTION) {
					txtAuthor.setText("");
					txtBookNum.setText("");
					txtISBN.setText("");
					txtTitle.setText("");
					cbbClassification.setSelectedIndex(1);
					cbbPublisher.setSelectedIndex(1);
					this.repaint();
				} else this.dispose();
			}
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Occur error durring run: "+e.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
		}
	}

}
