package book.boundary;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.HeadlessException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import book.controller.BookController;
import entity.Book;
import entity.SimpleQuery;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Class này là lớp biên của chức năng cập nhật sách
 * @author Blej
 *
 */
public class UpdateBook extends JFrame {

	private ManageBookForm manageBookForm;
	private JPanel contentPane;
	private JTextField txtBookNum;
	private JTextField txtTitle;
	private JTextField txtAuthor;
	private JTextField txtISBN;
	private JComboBox cbbPublisher;
	private JComboBox cbbClassification;
	private ArrayList<String> errorMsgs;

	/**
	 * Create the frame.
	 */
	public UpdateBook(Book book, ManageBookForm manageBookForm) {
		this.manageBookForm = manageBookForm;
		setTitle("Update Book");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel label = new JLabel("Book Number:");
		
		txtBookNum = new JTextField();
		txtBookNum.setEditable(false);
		txtBookNum.setColumns(10);
		txtBookNum.setText(book.getBookNumber());
		
		JLabel label_1 = new JLabel("Title:");
		
		txtTitle = new JTextField();
		txtTitle.setColumns(10);
		txtTitle.setText(book.getTitle());
		
		JLabel label_2 = new JLabel("Author:");
		
		txtAuthor = new JTextField();
		txtAuthor.setColumns(10);
		txtAuthor.setText(book.getAuthor());
		
		JLabel label_3 = new JLabel("Publisher:");
		
		cbbPublisher = new JComboBox();
		addItemtoCbb(cbbPublisher, SimpleQuery.PUBLISHER, "publisherName");
		cbbClassification = new JComboBox();
		addItemtoCbb(cbbClassification, SimpleQuery.CLASSIFICATION, "classificationName");
		try {
			cbbPublisher.setSelectedItem(BookController.getInst().findNameById(SimpleQuery.PUBLISHER, book.getPublisherId()));
			cbbClassification.setSelectedItem(BookController.getInst().findNameById(SimpleQuery.CLASSIFICATION, book.getClassificationId()));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		JLabel label_4 = new JLabel("ISBN:");
		
		txtISBN = new JTextField();
		txtISBN.setColumns(10);
		txtISBN.setText(book.getISBN());
		
		JLabel label_5 = new JLabel("Classification:");
		
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSubmitActionPerformed();
			}
		});
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(label, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(txtBookNum, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(43)
							.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(txtTitle, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(30)
							.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(txtAuthor, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(20)
							.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(cbbPublisher, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(40)
							.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(txtISBN, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(1)
							.addComponent(label_5, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
							.addGap(4)
							.addComponent(cbbClassification, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(166)
							.addComponent(btnSubmit, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_contentPane.createSequentialGroup()
					.addContainerGap(24, Short.MAX_VALUE)
					.addGap(14)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(label))
						.addComponent(txtBookNum, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(label_1))
						.addComponent(txtTitle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(label_2))
						.addComponent(txtAuthor, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(label_3))
						.addComponent(cbbPublisher, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(label_4))
						.addComponent(txtISBN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(3)
							.addComponent(label_5))
						.addComponent(cbbClassification, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(btnSubmit)
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
		errorMsgs = new ArrayList<String>();
	}
	
	protected void btnSubmitActionPerformed() {
		errorMsgs.clear();
        try {
			if(checkInput()) {
				updateBook();
				manageBookForm.genTableContent(manageBookForm.getTable(), BookController.getInst().getBooks());
			}
			else {
				String error = "";
				for (String msg : errorMsgs) {
					error+=msg;
					error+="\n";
				}
				JOptionPane.showMessageDialog(null, error, "Error!", JOptionPane.ERROR_MESSAGE);
			}
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void updateBook() {
		try {
			Book book = new Book();
			int publisherId = BookController.getInst().findIdByName(SimpleQuery.PUBLISHER, (String) cbbPublisher.getSelectedItem());
			int classificationId = BookController.getInst().findIdByName(SimpleQuery.CLASSIFICATION, (String) cbbClassification.getSelectedItem());
			book.setPublisherId(publisherId);
			book.setClassificationId(classificationId);
			book.setBookNumber(txtBookNum.getText());
			book.setTitle(txtTitle.getText());
			book.setAuthor(txtAuthor.getText());
			book.setISBN(txtISBN.getText());
			if (BookController.getInst().updateBook(book)) {
				JOptionPane.showMessageDialog(this, "Success!", "Update success!", JOptionPane.INFORMATION_MESSAGE);
			} else JOptionPane.showMessageDialog(this, "Error updating book!", "Error!", JOptionPane.ERROR_MESSAGE);
			this.dispose();
		} catch (HeadlessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Occur error durring run: "+e.toString(),"Error!", JOptionPane.ERROR_MESSAGE);
		} catch (SQLException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(this, "Occur error when connect database","Error!", JOptionPane.ERROR_MESSAGE);
		}
	}

	private boolean checkInput() throws ClassNotFoundException, SQLException {
		boolean hasError = false;
		String bookNumber = txtBookNum.getText();
		if (txtTitle.getText().equals("")) {
			errorMsgs.add("Title can't be blank!");
			hasError = true;
		}
		if (txtAuthor.getText().equals("")) {
			errorMsgs.add("Author can't be blank!");
			hasError = true;
		}
		if (txtISBN.getText().equals("")) {
			errorMsgs.add("ISBN can't be blank!");
			hasError = true;
		}
		return !hasError;
	}
	private void addItemtoCbb(JComboBox<String> comboBox, int tableName, String column) {
		ArrayList<String> items = new ArrayList<String>();
		SimpleQuery sq = new SimpleQuery();
		try {
			items = sq.getAll(tableName, column);
			for (String item : items) {
				comboBox.addItem(item);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
