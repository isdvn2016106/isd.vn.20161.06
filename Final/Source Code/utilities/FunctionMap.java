package utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import entity.Function;


public class FunctionMap {
		
	private Map<String, ArrayList<Function>> functionMap;
	private FunctionMap() {
		initialize();
	}
	private static FunctionMap inst;
	public static FunctionMap getInst() {
		if (inst == null)
			inst = new FunctionMap();
		return inst;
	}
	private void initialize() {
		functionMap = new HashMap<String,ArrayList<Function>>();
		
		ArrayList<Function> functionsAdmin = new ArrayList<Function>();
		functionsAdmin.add(new Function("Quản lý người dùng", user.boundary.ManageUserForm.class.getName()));
		functionMap.put("Admin", functionsAdmin);
		
		ArrayList<Function> functionsBorrower = new ArrayList<Function>();
		functionsBorrower.add(new Function("Đăng ký mượn sách", borrowreturn.boundary.RegisterToBorrowForm.class.getName()));
		functionsBorrower.add(new Function("Kích hoạt tài khoản", user.boundary.ActivateAccountForm.class.getName()));
		functionMap.put("Borrower", functionsBorrower);
		
		ArrayList<Function> functionsLibrarian = new ArrayList<Function>();
		functionsLibrarian.add(new Function("Nhận đăng ký", borrowreturn.boundary.AcceptBorrowForm.class.getName()));
		functionsLibrarian.add(new Function("Nhận trả", borrowreturn.boundary.AcceptReturnForm.class.getName()));
		functionsLibrarian.add(new Function("Quản lý sách", book.boundary.ManageBookForm.class.getName()));
		functionsLibrarian.add(new Function("Quản lý thẻ", user.boundary.ManageCardForm.class.getName()));
		functionMap.put("Librarian", functionsLibrarian);
	}
	public Map<String, ArrayList<Function>> getFunctionMap() {
		return functionMap;
	}
	
}
