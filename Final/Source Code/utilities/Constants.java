package utilities;

public class Constants {
	public static final int LOGIN_BLOCKED = 0;
	public static final int LOGIN_CHANGE_PASSWORD = 1;
	public static final int LOGIN_SUCCESS = 2;
	public static final int LOGIN_WRONG_PASSWORD = 3;
	public static final int LOGIN_WRONG_EMAIL = 4;
	
	public static final int CARD_EXPIRED = 0;
	public static final int HAS_UNRETURNED_BOOK = 1;
	public static final int REGISTER_TO_BORROW_SUCCESS = 2;
	public static final int REGISTER_TO_BORROW_FAILED = 3;
}
