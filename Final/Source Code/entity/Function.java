package entity;

/**
 * Class này là class chức năng của phần mềm
 * @author Sơn
 *
 */
public class Function{
	private String functionName;
	private String functionClassName;
	
	public Function(String name, String className) {
		this.functionName = name;
		this.functionClassName = className;
	}

	public String getFunctionName() {
		return functionName;
	}

	public String getFunctionClassName() {
		return functionClassName;
	}
	
}
