package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Class này quản lý BookCopy trong cơ sở dữ liệu
 * @author Blej
 *
 */
public class BookCopy extends DataAccessHelper{
	public final String SEARCH_BOOK_COPY_BY_COPY_ID = "select * from `bookcopy` where copyId = ?";
	private static final String SEARCH_COPY_BY_COPYID = "SELECT * FROM `bookCopy` WHERE copyID = ?";
	private static final String ADD_COPY = "INSERT INTO `bookCopy` (copyId, copyType, price, bookId, status) VALUES (?,?,?,?,?)";
	private static final String COUNT_COPY_BY_BOOKNUMBER = "select count(*) from `bookCopy` where bookId LIKE ?";
	private static final String COUNT_BORROWABLE = "select count(*) from `bookCopy` where bookId like ? and status like 'Available'";
	private static final String DELETE_COPY = "DELETE FROM `bookCopy` WHERE bookId like ?";
	private static final String GET_PRICE_BY_BOOKNUMBER = "SELECT price FROM `bookCopy` WHERE bookId LIKE ?";
	private static final String GET_COPY_BY_BOOKNUMBER = "SELECT * from `bookCopy` WHERE bookId LIKE ? AND status LIKE 'Available'";
	private static final String UPDATE_COPY = "UPDATE `bookCopy` SET `copyType` = ?, `price` = ?, `status` = ? WHERE copyID LIKE ?";
	private static final String UPDATE_COPY_MAKE_AVAILABLE = "UPDATE `bookCopy` SET status = ? WHERE copyID LIKE ?";
	private String copyId;
	private String copyType;
	private int price;
	private String status;
	private String bookId;
	
	/**
	 * Hàm này dùng để tìm kiếm bản sao theo mã bản sao
	 * @param copyId là mã bản sao
	 * @return kết quả tìm
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean findByCopyId(String copyId) throws ClassNotFoundException, SQLException {
		connectDB();
    	PreparedStatement ps = conn.prepareStatement(SEARCH_BOOK_COPY_BY_COPY_ID);
    	ps.setString(1, copyId + "");
    	ResultSet rs = ps.executeQuery();
    	if (rs != null && rs.next()) {
    		this.setBookId(rs.getString("bookId"));
    		this.setCopyId(rs.getString("copyId"));
    		this.setCopyType(rs.getString("copyType"));
    		this.setPrice(rs.getInt("price"));
    		this.setStatus(rs.getString("status"));
    		closeDB();
    		return true;
    	}
    	closeDB();
    	return false;
	}

	/**
	 * Hàm này tìm kiểm bản sao theo mã bản sao
	 * @param id là mã bản sao
	 * @return kết quả tìm được
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean findCopyByID(String id) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(SEARCH_COPY_BY_COPYID);
		ps.setString(1, id);
		ResultSet rs = ps.executeQuery();
		if(rs !=null && rs.next()){
			this.setBookId(rs.getString("bookId"));
			this.setCopyId(id);
			this.setCopyType(rs.getString("copyType"));
			this.setPrice(rs.getInt("price"));
			this.setStatus(rs.getString("status"));
            closeDB();
            return true;
        }
        closeDB();
        return false;
	}
	
	/**
	 * Hàm này dùng để thêm bản sao vào cơ sở dữ liệu
	 * @return kết quả thêm
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean addCopy() throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(SEARCH_COPY_BY_COPYID);
		ps.setString(1, copyId);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			closeDB();
			return false;
		} else {
			ps = conn.prepareStatement(ADD_COPY);
			ps.setString(1, copyId);
			ps.setString(2, copyType);
			ps.setInt(3, price);
			ps.setString(4, bookId);
			ps.setString(5, status);
			//System.out.println(ps.toString());
			ps.executeUpdate();
		}
		closeDB();
		return true;
	}
	/**
	 * Hàm này trả về số lượng bản sao theo mã sách
	 * @param bookNumber là mã sách
	 * @return số lượng bản sao
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int getCopyCountByBookNumber(String bookNumber) throws ClassNotFoundException, SQLException {
		int count = 0;
		connectDB();
		PreparedStatement ps = conn.prepareStatement(COUNT_COPY_BY_BOOKNUMBER);
		ps.setString(1, bookNumber);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			count = rs.getInt(1);
		}
		closeDB();
		return count;
	}
	/**
	 * Hàm này trả về số bản sao có thể cho mượn được theo mã sách
	 * @param bookNumber là mã sách
	 * @return số bản sao có thể cho mượn được
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int getBorrowableCopyCount(String bookNumber) throws ClassNotFoundException, SQLException {
		int count = 0;
		connectDB();
		PreparedStatement ps = conn.prepareStatement(COUNT_BORROWABLE);
		ps.setString(1, bookNumber);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			count = rs.getInt(1);
		}
		closeDB();
		return count;
	}
	/**
	 * Hàm này dùng để xóa một bản sao
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void deleteCopy() throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(DELETE_COPY);
		ps.setString(1, bookId);
		ps.executeUpdate();
		closeDB();
	}
	/**
	 * Hàm này trả về giá tiền của một bản sao theo mã sách
	 * @param bookNumber là mã sách
	 * @return giá tiền của bản sao
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int getPriceByBookNumber(String bookNumber) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_PRICE_BY_BOOKNUMBER);
		ps.setString(1, bookNumber);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			return rs.getInt("price");
		}
		closeDB();
		return 0;
	}
	/**
	 * Hàm này trả về một bản sao của một mã sách
	 * @param bookNumber là mã sách
	 * @return một bản sao
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean getACopyByBookNumber(String bookNumber) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_COPY_BY_BOOKNUMBER);
		ps.setString(1, bookNumber);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			this.setBookId(bookNumber);
			this.setCopyId(rs.getString("copyId"));
			this.setCopyType(rs.getString("copyType"));
			this.setPrice(rs.getInt("price"));
			this.setStatus(rs.getString("status"));
			closeDB();
			return true;
		}
		closeDB();
		return false;
	}
	/**
	 * Hàm này dùng để cập nhật thông tin bản sao
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void updateCopy() throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(UPDATE_COPY);
		ps.setString(1, copyType);
		ps.setInt(2, price);
		ps.setString(3, status);
		ps.setString(4, copyId);
		ps.executeUpdate();
		closeDB();
	}
	
	public String getCopyId() {
		return copyId;
	}
	public void setCopyId(String copyId) {
		this.copyId = copyId;
	}
	public String getCopyType() {
		return copyType;
	}
	public void setCopyType(String copyType) {
		this.copyType = copyType;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public void setBookId(String bookId) {
		this.bookId = bookId;
	}
	public String getBookId() {
		return bookId;
	}

	public void makeAvailable(String copyId) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(UPDATE_COPY_MAKE_AVAILABLE);
		ps.setString(1, "Available");
		ps.setString(2, copyId);
		ps.executeUpdate();
		closeDB();
	}
	
}
