package entity;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * Class này quản lý BorrowRegistration trong cơ sở dữ liệu
 * @author Sơn
 *
 */
public class BorrowRegistration extends DataAccessHelper{
	private final String SEARCH_BORROW_REGISTRATION = "select borrowId,registeredDate,lentDate,expectedReturnDate,returnedDate,compensation,cardId,status from `borrowregistration` where";
	private static final String GET_REGISTRATION_BY_USERNAME = "SELECT returnedDate, status FROM `borrowRegistration`,`borrowCard`,`userAccount` "
															 + " WHERE borrowRegistration.cardId = borrowCard.cardId AND borrowCard.userId = userAccount.userId"
															 + " AND userAccount.userName LIKE ?";
	private static final String ADD_REGISTRATION = "INSERT INTO `borrowRegistration` (registeredDate, compensation, cardId) VALUES (?,?,?)";
	private static final String UPDATE_STATUS = "update `borrowregistration` set status = ? where borrowId = ?";
	private static final String UPDATE_DATE_LENT_EXPECT_DATE = "update `borrowregistration` set lentDate = ?, expectedReturnDate = ? where cardId = ?";
	private static final String GET_DECLINED_REGISTRATION = "SELECT * from `borrowregistration` WHERE status = -1 AND cardid = ?";
	private static final String UPDATE_RETURN_DATE = "update `borrowregistration` set returnedDate = ? where borrowId = ?";
	private static final String GET_REGISTRATION = "SELECT * from `borrowregistration` WHERE borrowid = ?";

	private int borrowId;
	private Timestamp registeredDate;
	private Timestamp lentDate;
	private Timestamp expectedReturnDate;
	private Timestamp returnedDate;
	private int compensation;
	private int cardId;
	private int status;
	
	/**
	 * Hàm này cập nhật trang thái của đăng ký mượn sách
	 * @param status 1 là được chấp nhận, -1 là không được chấp nhận
	 * @param borrowId là id của thẻ mượn
	 * @return kết quả cập nhật
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int updateStatus(int status, int borrowId) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(UPDATE_STATUS);
		ps.setString(1, status + "");
		ps.setString(2, borrowId + "");
		int result =  ps.executeUpdate();
		closeDB();
		return result;
	}
	
	/**
	 * Hàm này cập nhật ngày mượn trả của đăng ký mượn sách
	 * @param lentDate là ngày cho mượn 
	 * @param expectedReturnDate là ngày trả dự kiến
	 * @param cardId là id cua thẻ
	 * @return kết quả cập nhật
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int updateLentDateAndExpectedReturnDate(Timestamp lentDate, 
			Timestamp expectedReturnDate, int cardId) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(UPDATE_DATE_LENT_EXPECT_DATE);
		ps.setTimestamp(1, lentDate);
		ps.setTimestamp(2, expectedReturnDate);
		ps.setString(3, cardId + "");
		int result = ps.executeUpdate();
		closeDB();
		return result;
	}
	
	/**
	 * Hàm này trả về thông tin mượn sách của một thẻ
	 * @param cardId là id của thẻ
	 * @return một ArrayList các thông tin mượn sách
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<BorrowRegistration> searchBorrowRegistrationByCardId(int status, int cardId) throws ClassNotFoundException, SQLException {
		ArrayList<BorrowRegistration> list = new ArrayList<BorrowRegistration>();
		ResultSet rs;
		String queryCardId = " status = ? AND cardId like ? ";
		String query = SEARCH_BORROW_REGISTRATION;
		query += queryCardId;
		connectDB();
        PreparedStatement ps = conn.prepareStatement(query);
        ps.setInt(1, status);
        ps.setString(2, "%" + cardId + "%");
        rs = ps.executeQuery();
        
        while (rs.next()) {
        	BorrowRegistration borrowRegistration = new BorrowRegistration();
        	borrowRegistration.setBorrowId(rs.getInt("borrowId"));
        	borrowRegistration.setCardId(rs.getInt("cardId"));
        	borrowRegistration.setRegisteredDate(rs.getTimestamp("registeredDate"));
        	borrowRegistration.setLentDate(rs.getTimestamp("lentDate"));
        	borrowRegistration.setExpectedReturnDate(rs.getTimestamp("expectedReturnDate"));
        	borrowRegistration.setReturnedDate(rs.getTimestamp("returnedDate"));
        	borrowRegistration.setCompensation(rs.getInt("compensation"));
        	borrowRegistration.setStatus(rs.getInt("status"));
        	list.add(borrowRegistration);
        }
        closeDB();
		return list;
	}
	
	/* Getters and setters */
	public int getBorrowId() {
		return borrowId;
	}
	public void setBorrowId(int borrowId) {
		this.borrowId = borrowId;
	}
	public Timestamp getRegisteredDate() {
		return registeredDate;
	}
	public void setRegisteredDate(Timestamp registeredDate) {
		this.registeredDate = registeredDate;
	}
	public Timestamp getLentDate() {
		return lentDate;
	}
	public void setLentDate(Timestamp lentDate) {
		this.lentDate = lentDate;
	}
	public Timestamp getReturnedDate() {
		return returnedDate;
	}
	public void setReturnedDate(Timestamp returnedDate) {
		this.returnedDate = returnedDate;
	}
	public int getCompensation() {
		return compensation;
	}
	public void setCompensation(int compensation) {
		this.compensation = compensation;
	}

	public int getCardId() {
		return cardId;
	}
	
	public void setCardId(int cardId) {
		this.cardId = cardId;
	}

	public Timestamp getExpectedReturnDate() {
		return expectedReturnDate;
	}

	/**
	 * Hàm này kiểm tra xem người dùng còn sách chưa trả không
	 * @param userName là tên tài khoản
	 * @return kết quả
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean checkReturned(String userName) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_REGISTRATION_BY_USERNAME);
		ps.setString(1, userName);
		ResultSet rs = ps.executeQuery();
		if (rs != null) {
			while (rs.next()) {
				int status = rs.getInt("status");
				Timestamp returnedDate = rs.getTimestamp("returnedDate");
				if (status == 1 && returnedDate == null) {
					closeDB();
					return false;
				}
			}
		}
		closeDB();
		return true;
	}

	/**
	 * Hàm này thêm vào một đăng ký mượn sách mới
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void addNewRegistration() throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(ADD_REGISTRATION, Statement.RETURN_GENERATED_KEYS);
		ps.setTimestamp(1, registeredDate);
		ps.setInt(2, compensation);
		ps.setInt(3, cardId);
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		if (rs.next()) {
			this.setBorrowId(rs.getInt(1));
		}
		closeDB();
	}

	public void setExpectedReturnDate(Timestamp expectedReturnDate) {
		this.expectedReturnDate = expectedReturnDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Hàm này tìm một đăng ký không được chấp nhận của người dùng
	 * @param cardId là id của thẻ
	 * @return kết quả tìm
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public boolean getDeclinedRegistrationByCardID(int cardId) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(GET_DECLINED_REGISTRATION);
		ps.setInt(1, cardId);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			this.setBorrowId(rs.getInt("borrowId"));
			closeDB();
			return true;
		}
		closeDB();
		return false;
	}

	/**
	 * Hàm này cập nhật ngày trả cho đăng ký
	 * @param returnDate là ngày trả
	 * @param borrowId là id của đăng ký
	 * @return kết quả cập nhật
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int updateReturnDate(Timestamp returnDate, int borrowId) throws ClassNotFoundException, SQLException {
		connectDB();
		PreparedStatement ps = conn.prepareStatement(UPDATE_RETURN_DATE);
		ps.setTimestamp(1, returnDate);
		ps.setInt(2, borrowId);
		int result = ps.executeUpdate();
		closeDB();
		return result;
	}

}
