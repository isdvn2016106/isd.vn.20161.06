package borrowreturn.boundary;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Hàm này là lớp biên của chức năng nhận mượn sách
 * @author Tùng
 *
 */
public class AcceptBorrowForm extends JPanel{
	private String username;
	public AcceptBorrowForm(String username) {
		this.username = username;
		SearchBorrowForm sbf = new SearchBorrowForm();
		sbf.setVisible(true);
		sbf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
}
