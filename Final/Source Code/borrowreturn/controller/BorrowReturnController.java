package borrowreturn.controller;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import book.controller.BookController;
import entity.BookCopy;
import entity.BorrowDetail;
import entity.BorrowRegistration;
import user.controller.CardController;
import utilities.Constants;

public class BorrowReturnController {
	private BorrowReturnController() {
		
	}
	private static BorrowReturnController inst;
	public static BorrowReturnController getInst() {
		if (inst == null) 
			inst = new BorrowReturnController();
		return inst;
	}
	
	/**
	 * Hàm này để thêm một đăng ký mượn sách vào cơ sở dữ liệu
	 * @param selectedBooks là một ArrayList các mã sách
	 * @param userName là username của người muốn mượn sách
	 * @return kết quả thêm vào cơ sở dữ liệu
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int registerToBorrow(ArrayList<String> selectedBooks, String userName) throws ClassNotFoundException, SQLException {
		if (!CardController.getInst().checkCard(userName)) return Constants.CARD_EXPIRED;
		if (!BorrowReturnController.getInst().checkReturned(userName)) return Constants.HAS_UNRETURNED_BOOK;
		
		BorrowRegistration registration = new BorrowRegistration();
		registration.setRegisteredDate(new Timestamp(new Date().getTime()));
		registration.setCardId(CardController.getInst().getCardByUserName(userName).getCardId());
		registration.setCompensation(calculateCompensation(selectedBooks));
		registration.addNewRegistration();
		
		BorrowDetail detail = new BorrowDetail();
		detail.setBorrowId(registration.getBorrowId());
		for (String bookNumber : selectedBooks) {
			BookCopy copy = BookController.getInst().getACopyByBookNumber(bookNumber);
			if (copy == null) return Constants.REGISTER_TO_BORROW_FAILED;
			detail.setCopyId(copy.getCopyId());
			detail.addDetail();
			copy.setStatus("Unavailable");
			copy.updateCopy();
		}
		return Constants.REGISTER_TO_BORROW_SUCCESS;
	}
	
	private int calculateCompensation(ArrayList<String> selectedBooks) {
		int result = 0;
		try {
			for (String bookNumber : selectedBooks) {
				result += BookController.getInst().getPriceByBookNumber(bookNumber);
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	private boolean checkReturned(String userName) throws ClassNotFoundException, SQLException {
		BorrowRegistration registation = new BorrowRegistration();
		return registation.checkReturned(userName);
	}
	
	/**
	 * Hàm này trả về thông tin mượn sách theo mã thẻ
	 * @param cardId là mã thẻ
	 * @return một ArrayList các đăng ký mượn sách
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<BorrowRegistration> searchBorrowRegistrationByCardId(int status, int cardId) throws ClassNotFoundException, SQLException {
		BorrowRegistration borrowRegistration = new BorrowRegistration();
		return borrowRegistration.searchBorrowRegistrationByCardId(status, cardId);
	}
	
	/**
	 * Hàm này dùng để cập nhật thông tin chấp nhận hay không chấp nhận cho một đăng ký mượn sách
	 * @param status 1 là chấp nhận, 2 là không chấp nhận
	 * @param borrowId là mã thẻ
	 * @return là kết quả cập nhật
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int updateBorrowRegistrationStatus(int status, int borrowId) throws ClassNotFoundException, SQLException {
		BorrowRegistration borrowRegistration = new BorrowRegistration();
		return borrowRegistration.updateStatus(status, borrowId);
	}
	
	/**
	 * Hàm này để cập nhật thông tin về ngày mượn trả của một đăng ký mượn sách
	 * @param lentDate là ngày mượn
	 * @param expectedReturnDate là ngày dự kiến phải trả
	 * @param cardId là mã thẻ
	 * @return kết quả cập nhật
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int updateBorrowRegistrationLentDateAndExpectedReturnDate(Timestamp lentDate, 
			Timestamp expectedReturnDate, int cardId) throws ClassNotFoundException, SQLException {
		BorrowRegistration borrowRegistration = new BorrowRegistration();
		return borrowRegistration.updateLentDateAndExpectedReturnDate(lentDate, expectedReturnDate, cardId);
	}

	/**
	 * Hàm này dùng để set trạng thái của bản sao thành available nếu như thủ thư không chấp nhận đăng ký mượn sách
	 * @param cardId là id của thẻ
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void makeDeclinedCopyAvailable(int cardId) throws ClassNotFoundException, SQLException {
		BorrowRegistration registration = getDeclinedRegistrationByCardID(cardId);
		if (registration != null) {
			ArrayList<BorrowDetail> details = getDetails(registration.getBorrowId());
			for (BorrowDetail detail : details) {
				BookController.getInst().makeCopyAvailable(detail.getCopyId());
			}
		}
		
	}

	/**
	 * Hàm này trả về toàn bộ chi tiêt của một đăng ký mượn sachs
	 * @param borrowId là id của đăng ký mướn sách
	 * @return một ArrayList các chi tiết
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public ArrayList<BorrowDetail> getDetails(int borrowId) throws ClassNotFoundException, SQLException {
		BorrowDetail detail = new BorrowDetail();
		return detail.getDetails(borrowId);
	}

	/**
	 * Hàm này trả về đăng ký bị từ chối của người dùng
	 * @param cardId là id của thẻ
	 * @return một đăng ký bị từ chối
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public BorrowRegistration getDeclinedRegistrationByCardID(int cardId) throws ClassNotFoundException, SQLException {
		BorrowRegistration registration = new BorrowRegistration();
		if (registration.getDeclinedRegistrationByCardID(cardId)) return registration;
		return null;
	}

	/**
	 * Hàm này cập nhật thời gian trả sách của một đăng ký
	 * @param returnDate là thời gian trả sách
	 * @param borrowId là id của đăng ký
	 * @return kết quả cập nhật
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int updateBorrowRegistrationReturnDate(Timestamp returnDate, int borrowId) throws ClassNotFoundException, SQLException {
		BorrowRegistration borrowRegistration = new BorrowRegistration();
		return borrowRegistration.updateReturnDate(returnDate, borrowId);
	}

	/**
	 * Hàm này cập nhật trạng thái available cho những bản sao được trả
	 * @param borrowId là id của dăng ký mượn sách
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void makeCopyAvailable(int borrowId) throws ClassNotFoundException, SQLException {
		ArrayList<BorrowDetail> details = getDetails(borrowId);
		for (BorrowDetail detail : details) {
			BookController.getInst().makeCopyAvailable(detail.getCopyId());
		}
	}

}
